#######################################################
#
# Example environment script to build example MyMarlin
#  
#  F. Gaede, DESY
#######################################################


# modify as needed:
export MARLIN=/afs/desy.de/group/it/ilcsoft/v01-06/Marlin/v00-10-04/

# use the same env.sh that has been used to build the Marlin library

. $MARLIN/env.sh 
