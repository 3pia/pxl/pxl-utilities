#include "PxlProcessor.h"
#include <iostream>

#ifdef MARLIN_USE_AIDA
#include <marlin/AIDAProcessor.h>
#include <AIDA/IHistogramFactory.h>
#include <AIDA/ICloud1D.h>
//#include <AIDA/IHistogram1D.h>
#endif

#include <EVENT/LCCollection.h>
#include <EVENT/MCParticle.h>
#include <EVENT/ReconstructedParticle.h>

// ----- include for verbosity dependend logging ---------
#include "marlin/VerbosityLevels.h"
#include "PxlBase.h"


using namespace lcio ;
using namespace marlin ;


PxlProcessor aPxlProcessor ;


PxlProcessor::PxlProcessor() : Processor("PxlProcessor") {
  
  // modify processor description
  _description = "PxlProcessor does whatever it does ..." ;
  

  // register steering parameters: name, description, class-variable, default value

  registerInputCollection( LCIO::MCPARTICLE,
			   "CollectionName" , 
			   "Name of the MCParticle collection"  ,
			   _colNameMC ,
			   std::string("MCParticle") ) ;

  registerInputCollection( LCIO::RECONSTRUCTEDPARTICLE,
			   "CollectionName" , 
			   "Name of the ReconstructedParticle collection"  ,
			   _colNameReco ,
			   std::string("ReconstructedParticle") ) ;


  registerProcessorParameter("OUTPUT_FILE",
			     "Name of the pxl output file ",
			     _fileName,
                             std::string("Output_file_name"));


}


void PxlProcessor::init() { 

  streamlog_out(DEBUG) << "   init called  " 
		       << std::endl ;
  

  // usually a good idea to
  printParameters() ;

  _nRun = 0 ;
  _nEvt = 0 ;

  pxlFile_ = new pxl::OutputFile(_fileName);
  
}

void PxlProcessor::processRunHeader( LCRunHeader* run) { 

  _nRun++ ;
} 

void PxlProcessor::processEvent( LCEvent * evt ) { 

    
  // this gets called for every event 
  // usually the working horse ...

  //Pxl part

   std::string pxlname = "default";
   //   std::map<int, std::string>::const_iterator found = pdgMap_.find(100);
   //   if (found!=pdgMap_.end())
   //      pxlname = found->second;
      

   pxl::Event event;
   
   // create two pxl::EventViews for Generator/Reconstructed Objects
   pxl::EventView* recEvtView = event.create<pxl::EventView>();
   pxl::EventView* genEvtView = event.create<pxl::EventView>();
      
   event.setIndex("Gen", genEvtView);
   event.setIndex("Rec", recEvtView);

   recEvtView->setName("Reconstructed");
   genEvtView->setName("Generator");
   
   genEvtView->setUserRecord<std::string>("Type", "Gen");
   recEvtView->setUserRecord<std::string>("Type", "Rec");
   // set physics process
   genEvtView->setUserRecord<std::string>("Process", process_);
   recEvtView->setUserRecord<std::string>("Process", process_);

   
  LCCollection* col_mc = evt->getCollection( _colNameMC ) ;
  LCCollection* col_reco = evt->getCollection( _colNameReco ) ;
  
  // looks like reco and mc collections contain the same info for MCOarticle


  if( col_mc != 0 ){
    

    int nMCP = col_mc->getNumberOfElements()  ;
    //    std::cout << "Number of elements in Collection MC = " <<  nMCP << std::endl;
    
    for(int i=0; i< nMCP ; i++){
      
      MCParticle* p = dynamic_cast<MCParticle*>( col_mc->getElementAt( i ) ) ;
      
      //      std::cout << "energy of gen part = " << p->getEnergy() << std::endl;
      
    } 

  }

  if( col_reco != 0 ){
    

    int nRecoP = col_reco->getNumberOfElements()  ;

    //    std::cout << "Number of elements in Collection Reco = " <<  nRecoP << std::endl;


    for(int i=0; i< nRecoP ; i++){
    
      // ReconstructedParticle does not work ???
  
      MCParticle* p2 = dynamic_cast<MCParticle*>( col_reco->getElementAt( i ) ) ;

      //      std::cout << "energy of reco part = " << p2->getEnergy() << std::endl;

      int p_pdg = p2->getPDG();
      //      const double mom[];
      if (p_pdg==13){
	//	std::cout << "Muon!" << std::endl;
	pxl::Particle* muon = recEvtView->create<pxl::Particle>();
	muon->setName("Muon");
	muon->setP4(p2->getMomentum()[0], p2->getMomentum()[1], p2->getMomentum()[2], p2->getEnergy());
      }

    } 

  }


  pxlFile_->writeEvent(&event);


  

  //-- note: this will not be printed if compiled w/o MARLINDEBUG=1 !

  streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber() 
		       << "   in run:  " << evt->getRunNumber() 
		       << std::endl ;
  





  _nEvt ++ ;
}



void PxlProcessor::check( LCEvent * evt ) { 
  // nothing to check here - could be used to fill checkplots in reconstruction processor
}


void PxlProcessor::end(){ 
  
  delete pxlFile_;
  

//   std::cout << "PxlProcessor::end()  " << name() 
// 	    << " processed " << _nEvt << " events in " << _nRun << " runs "
// 	    << std::endl ;

}

