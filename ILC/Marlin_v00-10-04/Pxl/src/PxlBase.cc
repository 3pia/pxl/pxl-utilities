//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------

#include <iostream>

#include "PxlBase.h"

namespace pxl
{

void ObjectOwner::init(const pxl::ObjectOwner& original)
{
	// copy objects: loop in STL style
	for (const_iterator iter = original._container.begin(); iter
			!= original._container.end(); iter++)
	{

		pxl::Relative* pOld = *iter;
		pxl::Relative* pNew = pOld->clone();

		set(pNew);

		pxl::CopyHistory::iterator insertPos =
				_copyHistory.lower_bound(pOld->id());
		if (insertPos == _copyHistory.end() || insertPos->first != pOld->id())
			_copyHistory.insert(insertPos,
					pxl::CopyHistory::iterator::value_type(pOld->id(), pNew));
		else
			insertPos->second = pNew;
	}

	// FIXME: possibly inefficient, might be done all in one loop
	// redirect relations: loop in PTL style
	for (const_iterator iter = original._container.begin(); iter
			!=original._container.end(); iter++)
	{
		pxl::Relative* pOld = *iter;
		pxl::Relative* pNew = 0;
		pxl::CopyHistory::const_iterator found = _copyHistory.find(pOld->id());
		if (found != _copyHistory.end())
			pNew = found->second;

		// mother relations
		for (pxl::Relations::const_iterator iter = pOld->getMotherRelations()->begin(); iter!=pOld->getMotherRelations()->end(); ++iter)
		{
			pxl::Relative* pOldRel = *iter;
			pxl::Relative* pNewRel = 0;
			pxl::CopyHistory::const_iterator foundRel =
					_copyHistory.find(pOldRel->id());
			if (foundRel != _copyHistory.end())
				pNewRel = foundRel->second;

			if (pOldRel)
			{
				if (pNewRel)
					pNew->linkMother(pNewRel);
				else
					// FIXME: cerr again?
					std::cerr
							<< "pxl::ObjectOwner::ObjectOwner(...): WARNING: some original objects had relations to objects of other owners."
							<< std::endl;
			}
			else
				std::cerr
						<< "pxl::ObjectOwner::ObjectOwner(...): WARNING: some originally related objects no longer exist."
						<< std::endl;
		}

		// daughter relations
		// have been set automatically above
	}

	// redirect index:
	for (pxl::Index::const_iterator iter = original._index.begin(); iter
			!=original._index.end(); ++iter)
	{

		pxl::Relative* pOld = iter->second;

		pxl::Relative* pNew = 0;
		pxl::CopyHistory::const_iterator found = _copyHistory.find(pOld->id());
		if (found != _copyHistory.end())
			pNew = found->second;

		if (pNew)
			_index.insert(pxl::Index::const_iterator::value_type(iter->first,
					pNew));
		else
			std::cerr
					<< "pxl::ObjectOwner::ObjectOwner(...): WARNING: some original indices pointed to objects of other owners."
					<< std::endl;
	}
}

void ObjectOwner::clearContainer()
{
	for (iterator iter = _container.begin(); iter != _container.end(); iter++)
	{
		(*iter)->_refObjectOwner=0;
		delete (*iter);
	}
	_container.clear();
	_copyHistory.clear();
	_index.clear();
	_uuidSearchMap.clear();
}

void ObjectOwner::set(pxl::Relative* item)
{
	item->_refObjectOwner = this;
	_container.push_back(item);
	_uuidSearchMap.insert(std::pair<pxl::Id, pxl::Relative*>(item->getId(), item));
}

void ObjectOwner::remove(pxl::Relative* item)
{
	// search & remove possible indices (multiple occurrences possible!)
	for (pxl::Index::const_iterator iter = _index.begin(); iter != _index.end(); iter++)
	{

		// FIXME: inefficient
		if (item == iter->second)
		_index.erase(iter->first);
	}

	// search & remove possible copy history
	for (pxl::CopyHistory::const_iterator iter = _copyHistory.begin(); iter
	!= _copyHistory.end(); iter++)
	{

		// FIXME: inefficient
		if (item == iter->second)
		{
			_copyHistory.erase(iter->first);
			break; // multiple occurrences *not* possible!
		}
	}

	_uuidSearchMap.erase(item->getId());

	item->_refObjectOwner=0;
	for (iterator iter = _container.begin(); iter != _container.end(); iter++)
	{

		if (item == (*iter))
		{
			delete *iter;
			_container.erase(iter);
			break;
		}
	}

}

bool ObjectOwner::has(const pxl::Relative* item) const
{
	return item->_refObjectOwner == this;
}

void ObjectOwner::serialize(const OutputStream &out) const
{
	// contents of vector
	out.writeUnsignedInt(size());
	for (const_iterator iter = begin(); iter!=end(); ++iter)
	{
		(*iter)->serialize(out);

		//serialize relations explicitly
		(*iter)->getMotherRelations()->serialize(out);
		(*iter)->getDaughterRelations()->serialize(out);
		(*iter)->getFlatRelations()->serialize(out);
	}

	// index
	out.writeInt(_index.size());
	for (pxl::Index::const_iterator iter = _index.begin(); iter != _index.end(); ++iter)
	{
		out.writeString(iter->first);
		(iter->second->id()).serialize(out);
	}

}

void ObjectOwner::deserialize(const InputStream &in)
{
	/* Algorithm:
	 * a) deserialize all objects. those deserialize themselves, and their relations 
	 * as the related objects' uuids.
	 * b) create temprorary id-object map within the same loop.
	 * (no need (!) for orphan relations in this new algorithm)
	 * c) recreate relations (fetching objects from map)
	 * d) fill index (fetching objects from map)
	 * [e) shall the CopyHistory be serialised/deserialised? this is hard since
	 * one would need to have all ObjectOwners in memory, ie the whole event, and do some
	 * explicit stuff afterwards.]
	 * no error handling at the moment
	 */

	std::map<pxl::Id, pxl::Relative*> objIdMap;
	std::multimap<pxl::Relative*, pxl::Id> daughterRelationsMap;
	std::multimap<pxl::Relative*, pxl::Id> motherRelationsMap;
	std::multimap<pxl::Relative*, pxl::Id> flatRelationsMap;

	unsigned int size = 0;
	in.readUnsignedInt(size);
	for (unsigned int i=0; i<size; ++i)
	{
		pxl::Id typeId;
		typeId.deserialize(in);
		// Contained object must be a Relative derivative
		pxl::Relative* object = dynamic_cast<pxl::Relative*>(pxl::ObjectFactory::instance().create(typeId));
		object->deserialize(in);
		set(object);
		objIdMap.insert(std::pair<pxl::Id, pxl::Relative*>(object->id(), object));

		int msize = 0;
		in.readInt(msize);
		for (int j=0; j<msize;++j)
		{
			pxl::Id id;
			id.deserialize(in);
			motherRelationsMap.insert(std::pair<pxl::Relative*, pxl::Id>(object, id));
		}

		int dsize = 0;
		in.readInt(dsize);
		for (int j=0; j<dsize;++j)
		{
			pxl::Id id;
			id.deserialize(in);
			daughterRelationsMap.insert(std::pair<pxl::Relative*, pxl::Id>(object, id));
		}
		int fsize = 0;
		in.readInt(fsize);
		for (int j=0; j<fsize;++j)
		{
			pxl::Id id;
			id.deserialize(in);
			flatRelationsMap.insert(std::pair<pxl::Relative*, pxl::Id>(object, id));
		}

	}

	for (std::multimap<pxl::Relative*, pxl::Id>::const_iterator iter = daughterRelationsMap.begin();
	iter!=daughterRelationsMap.end(); ++iter)
	{
		pxl::Relative* target = objIdMap.find(iter->second)->second;
		iter->first->linkDaughter(target);
	}
	
	for (std::multimap<pxl::Relative*, pxl::Id>::const_iterator iter = flatRelationsMap.begin();
	iter!=flatRelationsMap.end(); ++iter)
	{
		pxl::Relative* target = objIdMap.find(iter->second)->second;
		iter->first->linkFlat(target);
	}

	

	in.readUnsignedInt(size);

	for (unsigned int i=0; i<size; ++i)
	{
		std::string name;
		in.readString(name);
		pxl::Id id;
		id.deserialize(in);
		_index.insert(std::pair<std::string, pxl::Relative*>(name, objIdMap.find(id)->second));
	}
}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------


namespace pxl
{

void pxl::Relations::serialize(const OutputStream &out) const
{
	out.writeInt(size());
	for (const_iterator iter = begin(); iter!=end(); ++iter)
	{
		(*iter)->getId().serialize(out);
	}
}

}
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl
{

void Relative::linkMother(pxl::Relative* target)
{
	if (target->_refObjectOwner != this->_refObjectOwner)
		exception("pxl::ObjectBase::linkDaughter(...)",
				"WARNING: mother and daughter have not the same object holder!");

	this->_motherRelations.set(target);
	target->_daughterRelations.set(this);
}

void Relative::linkDaughter(pxl::Relative* target)
{
	if (target->_refObjectOwner != this->_refObjectOwner)
		exception("pxl::ObjectBase::linkMother(...)",
				"WARNING: mother and daughter have not the same object holder!");

	this->_daughterRelations.set(target);
	target->_motherRelations.set(this);
}

void Relative::linkFlat(pxl::Relative* target)
{
	if (target->_refObjectOwner != this->_refObjectOwner)
		exception("pxl::ObjectBase::linkFlat(...)",
				"WARNING: mother and daughter have not the same object holder!");

	this->_flatRelations.set(target);
	target->_flatRelations.set(this);
}

void Relative::unlinkMother(pxl::Relative* target)
{
	this->_motherRelations.erase(target);
	target->_daughterRelations.erase(this);
}

void Relative::unlinkDaughter(pxl::Relative* target)
{
	this->_daughterRelations.erase(target);
	target->_motherRelations.erase(this);
}

void Relative::unlinkFlat(pxl::Relative* target)
{
	this->_flatRelations.erase(target);
	target->_flatRelations.erase(this);
}

void Relative::unlinkMothers()
{
	for (pxl::Relations::const_iterator iter = _motherRelations.begin(); iter
			!=_motherRelations.end(); ++iter)
	{
		(*iter)->_daughterRelations.erase(this);
	}

	_motherRelations.clearContainer();
}

void Relative::unlinkDaughters()
{
	for (pxl::Relations::const_iterator iter = _daughterRelations.begin(); iter
			!=_daughterRelations.end(); ++iter)
	{

		(*iter)->_motherRelations.erase(this);
	}

	_daughterRelations.clearContainer();
}

void Relative::unlinkFlat()
{
	for (pxl::Relations::const_iterator iter = _flatRelations.begin(); iter
			!=_flatRelations.end(); ++iter)
	{

		(*iter)->_flatRelations.erase(this);
	}

	_flatRelations.clearContainer();
}

std::ostream& Relative::printDecayTree(int level, std::ostream& os, int pan) const
{
	int daughters = 0;

	print(level, os, pan);

	for (pxl::Relations::const_iterator iter = _daughterRelations.begin(); iter
			!=_daughterRelations.end(); ++iter)
	{

		(*iter)->printDecayTree(level, os, pan + 1);
		daughters++;

	}

	if (daughters && pan > 1)
	{
		for (int p = 0; p < pan; p++)
			os << "|  ";
		os << "*" << std::endl;
	}

	return os;
}

std::ostream& Relative::print(int level, std::ostream& os, int pan) const
{
	return printPan1st(os, pan) << "pxl::ObjectBase with id: " << id()
			<< std::endl;
}

std::ostream& Relative::printPan1st(std::ostream& os, int pan) const
{
	for (int p = 0; p < pan - 2; p++)
		os << "|  ";
	if (pan - 1 > 0)
		os << "+--";
	if (pan)
		os << "{ ";
	return os;
}

std::ostream& Relative::printPan(std::ostream& os, int pan) const
{
	for (int p = 0; p < pan - 1; p++)
		os << "|  ";
	if (pan)
		os << "| ";
	return os;
}

} // namespace pxl

std::ostream& operator<<(std::ostream& cxxx, const pxl::Relative& obj)
{
	return obj.print(0, cxxx, 0);
}
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl
{
void SoftRelations::set(pxl::Relative* relative, const std::string& type)
{
	_relationsMap.insert(std::pair<std::string, pxl::Id>(type, relative->getId()));
}

void SoftRelations::remove(pxl::Relative* relative, const std::string& type)
{
	std::pair<iterator, iterator> iterators = _relationsMap.equal_range(type);
	for (iterator iter = iterators.first; iter!= iterators.second; ++iter)
	{
		if (iter->second==relative->getId())
		{
			_relationsMap.erase(iter);
			break;
		}
	}
}

pxl::Relative* SoftRelations::getFirst(const pxl::ObjectOwner* owner,
const std::string& type) const
{
	if (type=="")
	{
		const_iterator found = _relationsMap.begin();
		if (found!=_relationsMap.end())
		{
			return owner->getById(found->second);
		}
		else
		return 0;
	}
	else
	{
		const_iterator found = _relationsMap.find(type);
		if (found!=_relationsMap.end())
		{
			return owner->getById(found->second);
		}
		else
		return 0;
	}

}

int SoftRelations::getSoftRelatives(std::vector<pxl::Relative*>& vec,
const pxl::ObjectOwner* owner, const std::string& type) const
{
	int size = vec.size();
	if (type=="")
	{
		for (const_iterator iter = _relationsMap.begin(); iter
		!= _relationsMap.end(); ++iter)
		{
			pxl::Relative* relative = owner->getById(iter->second);
			if (relative!=0)
			vec.push_back(relative);
		}
	}
	else
	{
		std::pair<const_iterator, const_iterator> iterators =
		_relationsMap.equal_range(type);
		for (const_iterator iter = iterators.first; iter!= iterators.second; ++iter)
		{
			pxl::Relative* relative = owner->getById(iter->second);
			if (relative!=0)
			vec.push_back(relative);
		}
	}
	return vec.size()-size;

}

} //namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl
{

void UserRecord::serialize(const OutputStream &out) const
{
	out.writeUnsignedInt(getContainer()->size());
	for (const_iterator iter = getContainer()->begin(); iter!=getContainer()->end(); ++iter)
	{
		out.writeString(iter->first);
		pxl::Variant::Type type = iter->second.getType();

		char cType = ' ';
		switch (type)
		{
		case pxl::Variant::TYPE_BOOL:
			cType='b';
			out.writeChar(cType);
			out.writeBool(iter->second.get<bool>());
			break;
		case pxl::Variant::TYPE_CHAR:
			cType = 'c';
			out.writeChar(cType);
			out.writeChar(iter->second.get<char>());
			break;
		case pxl::Variant::TYPE_UCHAR:
			cType = 'C';
			out.writeChar(cType);
			out.writeUnsignedChar(iter->second.get<unsigned char>());
			break;
		case pxl::Variant::TYPE_INT:
			cType = 'i';
			out.writeChar(cType);
			out.writeInt(iter->second.get<int>());
			break;
		case pxl::Variant::TYPE_UINT:
			cType = 'I';
			out.writeChar(cType);
			out.writeUnsignedInt(iter->second.get<unsigned int>());
			break;
		case pxl::Variant::TYPE_SHORT:
			cType = 'o';
			out.writeChar(cType);
			out.writeShort(iter->second.get<short>());
			break;
		case pxl::Variant::TYPE_USHORT:
			cType = 'O';
			out.writeChar(cType);
			out.writeUnsignedShort(iter->second.get<unsigned short>());
			break;
		case pxl::Variant::TYPE_LONG:
			cType = 'l';
			out.writeChar(cType);
			out.writeLong(iter->second.get<long>());
			break;
		case pxl::Variant::TYPE_ULONG:
			cType = 'L';
			out.writeChar(cType);
			out.writeUnsignedLong(iter->second.get<unsigned long>());
			break;
		case pxl::Variant::TYPE_DOUBLE:
			cType = 'd';
			out.writeChar(cType);
			out.writeDouble(iter->second.get<double>());
			break;
		case pxl::Variant::TYPE_FLOAT:
			cType = 'f';
			out.writeChar(cType);
			out.writeFloat(iter->second.get<float>());
			break;
		case pxl::Variant::TYPE_STRING:
			cType = 's';
			out.writeChar(cType);
			out.writeString(iter->second.get<std::string>());
			break;
		case pxl::Variant::TYPE_USER:
			cType = 'u';
			out.writeChar(cType);
			break;
		case pxl::Variant::TYPE_PTR:
			cType = 'p';
			out.writeChar(cType);
			break;
		default:
			out.writeChar(cType);
			std::cerr << "Type not handled in pxl::Variant I/O." << std::endl;
		}

	}
}

void UserRecord::deserialize(const InputStream &in)
{
	unsigned int size = 0;
	in.readUnsignedInt(size);
	for (unsigned int j=0; j<size; ++j)
	{
		std::string name;
		in.readString(name);
		char cType;
		in.readChar(cType);
		//FIXME: temporary solution here - could also use static lookup-map,
		//but leave this unchanged until decided if to switch to new UR implementation.
		switch (cType)
		{
		case 'b':
		{
			bool b;
			in.readBool(b);
			set<bool>(name, b);
		}
			break;
		case 'c':
		{
			char c;
			in.readChar(c);
			set<char>(name, c);
		}
			break;
		case 'C':
		{
			unsigned char c;
			in.readUnsignedChar(c);
			set<unsigned char>(name, c);
		}
			break;

		case 'i':
		{
			int ii;
			in.readInt(ii);
			set<int>(name, ii);
		}
			break;
		case 'I':
		{
			unsigned int ui;
			in.readUnsignedInt(ui);
			set<unsigned int>(name, ui);
		}
			break;
		case 'o':
		{
			short s;
			in.readShort(s);
			set<short>(name, s);
		}
			break;
		case 'O':
		{
			unsigned short us;
			in.readUnsignedShort(us);
			set<unsigned short>(name, us);
		}
			break;
		case 'l':
		{
			long l;
			in.readLong(l);
			set<long>(name, l);
		}
			break;
		case 'L':
		{
			unsigned long ul;
			in.readUnsignedLong(ul);
			set<unsigned long>(name, ul);
		}
			break;
		case 'd':
		{
			double d;
			in.readDouble(d);
			set<double>(name, d);
		}
			break;
		case 'f':
		{
			float f;
			in.readFloat(f);
			set<float>(name, f);
		}
			break;
		case 's':
		{
			std::string ss;
			in.readString(ss);
			set<std::string>(name, ss);
		}
			break;
		case 'u':
		case 'p':
		default:
			std::cerr << "Type " << cType << " not handled in pxl::Variant I/O." << std::endl;
		}
	}
}

} //namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------

#include <cstddef>
#include <sstream>
#include <string>
#include <vector>


namespace pxl {

std::vector<pxl::VariantBase::TypeInfo> VariantBase::types;

/// This constant array serves the PXL variant data type. 
static const char *typeNames[] = { // Note: must match pxl::VariantBase::Type
    "null", "bool", "char", "uchar", "short", "ushort", "int", "uint",
    "long", "ulong", "float", "double", "string", "ptr"
};

const pxl::VariantBase::TypeInfo& VariantBase::fallbackGetTypeInfo(Type t)
{
    if (PXL_UNLIKELY(types.begin() == types.end())) {
        for(unsigned int i = 0; i < sizeof typeNames / sizeof *typeNames; i++)
            types.push_back(typeNames[i]);
    }

    if ((std::size_t)t >= types.size()) {
        std::ostringstream ss;
        ss << "Variant type " << (std::size_t)t << " undefined.";
        pxl::exception("pxl::VariantBase::fallbackGetTypeInfo", ss.str());
    }

    return types[t];
}

void VariantBase::wrongType(Type tShould, Type tIs) const
{
    const TypeInfo& is     = getTypeInfo(tIs);
    const TypeInfo& should = getTypeInfo(tShould);

    pxl::exception("pxl::VariantBase::wrongType",
                   std::string("Trying to access pxl::Variant of type \"")
                   + is.name + "\" as \"" + should.name + "\".");
}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------


namespace pxl {

void WkPtrBase::notifyDeleted()
{
    _objectRef = 0;
    if (_notifyChainOut)
        _notifyChainOut->notifyDeleted();
    _notifyChainIn = 0; 
    _notifyChainOut = 0;
}

void WkPtrBase::connect(pxl::Relative* pointer)
{
    // disconnect:
    if (_objectRef) {
        if (_objectRef->_refWkPtrSpec == this)
            _objectRef->_refWkPtrSpec = _notifyChainOut;
        if (_notifyChainIn && _notifyChainOut) {
            _notifyChainIn->_notifyChainOut = _notifyChainOut;
            _notifyChainOut->_notifyChainIn = _notifyChainIn;
        } else {
            if (_notifyChainIn)
                _notifyChainIn->_notifyChainOut = 0;
            if (_notifyChainOut)
                _notifyChainOut->_notifyChainIn = 0;
        }
    }   
    _notifyChainOut = 0;
    _notifyChainIn = 0; 

    // connect
    if (pointer) {
        _notifyChainIn = 0;
        _notifyChainOut = pointer->_refWkPtrSpec;
        if (_notifyChainOut)
            _notifyChainOut->_notifyChainIn = this;
        pointer->_refWkPtrSpec = this;
    }

    _objectRef = pointer;
}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------

#ifdef WIN32
#else
	#include <sys/time.h>
#endif


namespace pxl {

double getCpuTime()
{
#ifdef WIN32
	return timeGetTime() / 1000.;
#else
	struct timeval  tv;
	gettimeofday( &tv, NULL );
	return ( (double) tv.tv_sec + (double) tv.tv_usec / 1000000.0 );
#endif
}

void exception(const char* routine, const char* message)
{
    //FIXME: use logging facility instead of cerr
    std::cerr << routine << ": " << message << std::endl;
    std::cerr << "pxl::exception(): throwing pxl::Exception." << std::endl;
    throw pxl::Exception(routine, message);
}

//void exception(const std::string& routine, const std::string& message)
//{
//    std::cerr << routine << ": " << message << std::endl;
//    std::cerr << "pxl::exception(): throwing pxl::Exception." << std::endl;
//    throw pxl::Exception(routine, message);
//}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------


namespace pxl
{

bool ChunkReader::skip()
{
	//skip event header
	if (_status==0)
	{
		_stream.ignore(1);
		// read info size
		pxl::int32_t infoSize = 0;
		_stream.read((char *)&infoSize, 4);
		_stream.ignore(infoSize);
	}

	//skip all blocks
	while (nextBlockId()=='B' && !_stream.eof() )
	{
		// read info size
		pxl::int32_t infoSize = 0;
		_stream.read((char *)&infoSize, 4);
		_stream.ignore(infoSize);
		_stream.ignore(1);

		// read chunk size
		pxl::int32_t chunkSize = 0;
		_stream.read((char *)&chunkSize, 4);
		_stream.ignore(chunkSize);
	}

	_stream.ignore(4);
	_status=0;

	if (_stream.peek()==EOF)
		return false;
	return true;
}

bool ChunkReader::previous()
{
	std::streampos pos = _stream.tellg();
	pxl::int32_t eventSize;
	pos -= 4;
	if (pos<0)
			return false;
	_stream.seekg(pos);
	_stream.read((char*)&eventSize, 4);
	std::cerr << eventSize << std::endl;
	pos -= eventSize;
	if (pos<0)
		return false;
	_stream.seekg(pos);
	_status=0;
	return true;
}

/// Reads next block from file to stream. If mode is -1, the information condition string is evaluated,
/// i.e. the block is read only if the string equals the one in the input.
bool ChunkReader::nextBlockIf(int mode, const std::string& infoCondition)
{
	//if event header not read, return
	if (_status!=1)
		return false;

	//check if beginning of block
	char id = nextBlockId();

	if (id!='B')
	{
		if (id=='e') //end of event
		{
			_status=0;
			_stream.ignore(4);
			return false;
		}
		else
			std::cerr << "Unknown char identifier: " << id
					<< "in pxl::ChunkReader::nextBlockIf." << std::endl;
	}

	pxl::int32_t infoSize = 0;
	_stream.read((char *)&infoSize, 4);

	if (mode==-1)
	{
		char* infoBuffer = new char[infoSize];
		_stream.read(infoBuffer, infoSize);
		std::string info(infoBuffer);
		//the mode is set to -2 if the info condition is not fulfilled.
		//rest of block must be skipped and false be returned.
		if (mode==-1 && infoCondition!=info)
			mode=-2;
		delete infoBuffer;
	}
	else
		_stream.ignore(infoSize);

	char compressionMode;
	_stream.read(&compressionMode, 1);

	// read chunk size
	pxl::int32_t chunkSize = 0;
	_stream.read((char *)&chunkSize, 4);
	if (_stream.bad() || _stream.eof())
		return false;

	if (mode==0 || mode==-2)
		_stream.ignore(chunkSize);
	else
	{
		// read chunk into buffer
		if (compressionMode==' ')
		{
			_buffer.resize(chunkSize);
			_stream.read(_buffer.data(), chunkSize);
			if (_stream.bad() || _stream.eof() )
				return false;
		}
		else if (compressionMode=='Z')
		{
			unzipEventData(chunkSize);
		}
		else
		{
			pxl::exception("ChunkReader::nextBlockIf()",
					"Invalid compression mode.");
		}
	}

	if (mode==-2)
		return false;
	return true;
}

bool ChunkReader::nextEventIf(int mode, const std::string& infoCondition)
{
	if (_status!=0 || _stream.peek()==EOF || _stream.bad() )
		return false;

	if (mode==-3)
	{
		if (nextBlockId()!='I')
		{
			skip();
			return nextEventIf(mode, infoCondition);
		}
		else
			mode=-1;

	}
	else
	{
		if (nextBlockId()!='E')
		{
			skip();
			if (mode==-2)
				return false;
			else
				return nextEventIf(mode, infoCondition);
		}
	}
	pxl::int32_t infoSize = 0;
	_stream.read((char *)&infoSize, 4);

	if (_stream.eof() || _stream.bad() )
		return false;

	if (mode==-1)
	{
		char* infoBuffer = new char[infoSize];
		_stream.read(infoBuffer, infoSize);
		std::string info(infoBuffer);
		delete infoBuffer;
		if (mode==-1 && infoCondition!=info)
		{
			_status=1;
			skip();
			return false;
		}
	}
	else
		_stream.ignore(infoSize);

	_status=1; //event header read in

	if (_stream.eof() || _stream.bad() )
		return false;

	return true;
}

int ChunkReader::unzipEventData(int nBytes)
{
	int ret, length = 0;
	pxl::int32_t have;

	unsigned char* _inputBuffer =
			new unsigned char[iotl__iStreamer__lengthUnzipBuffer];
	unsigned char* _outputBuffer =
			new unsigned char[iotl__iStreamer__lengthUnzipBuffer];

	z_stream strm;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.avail_in = 0;
	strm.next_in = Z_NULL;

	ret = inflateInit(&strm);
	if (ret != Z_OK)
		return 0;

	// decompress until deflate stream ends or end of file
	do
	{
		int size = nBytes;
		if (size > iotl__iStreamer__lengthUnzipBuffer)
			size = iotl__iStreamer__lengthUnzipBuffer;

		strm.avail_in = _stream.read((char*)_inputBuffer, size).gcount();
		if (_stream.bad())
		{
			inflateEnd(&strm);
			return 0;
		}

		nBytes -= strm.avail_in;
		if (_stream.eof())
			nBytes = 0;

		if (strm.avail_in == 0)
			break;

		strm.next_in = _inputBuffer;

		// run inflate() on input until output buffer not full
		do
		{
			strm.avail_out = iotl__iStreamer__lengthUnzipBuffer;
			strm.next_out = _outputBuffer;

			ret = inflate(&strm, Z_NO_FLUSH);
			switch (ret)
			{
			case Z_STREAM_ERROR:
				pxl::exception("pxl::unzipEventData()",
						"Internal inflate stream error.");
			case Z_NEED_DICT:
				ret = Z_DATA_ERROR; // fall through
			case Z_DATA_ERROR:
			case Z_MEM_ERROR:
				inflateEnd(&strm);
				return 0;
			default:
				break;
			}

			have = iotl__iStreamer__lengthUnzipBuffer - strm.avail_out;

			length += have;

			_buffer.destroy();
			_buffer.resize(length);
			memcpy(_buffer.data(), _outputBuffer+(length-have), have);

		} while (strm.avail_out == 0);
	} while (nBytes > 0); // done when inflate() says it's done
	inflateEnd(&strm);

	delete[] _inputBuffer;
	delete[] _outputBuffer;

	return length;
}

}

//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------


namespace pxl
{

bool ChunkWriter::write(std::string info, char compressionMode)
{
	// write out block information
	const char* cInfo = info.c_str();
	pxl::int32_t lengthInfo = info.length();
	_stream.write((char *) &lengthInfo, 4);
	_nBytes+=4;
	_stream.write(cInfo, lengthInfo);
	_nBytes+=lengthInfo;

	// write out compression mode
	char compressed = 'Z';
	if (compressionMode == ' ') compressed = ' ';
	_stream.write((char *) &compressed, 1);
	_nBytes+=1;

	// zip block:
	const char* cBuffer = _buffer.getData();
	pxl::int32_t lengthBuffer = _buffer.getSize();

	const char* cZip = cBuffer;
	pxl::int32_t lengthZip = lengthBuffer;

	char* cZipSpace = 0;
	pxl::int32_t lengthZipSpace = 0;

	if (compressionMode == ' ')
	{
		// no compression requires no action...
	}
	else if (compressionMode >= '0' && compressionMode <= '9')
	{
		// data compression a la Gero, i.e. compression level = 6:
		lengthZipSpace = int(double(lengthBuffer) * 1.05 + 16);
		cZipSpace = new char[lengthZipSpace];

		int status = compress2((Bytef*)cZipSpace, (uLongf*)&lengthZipSpace,
				(const Bytef*)cBuffer, lengthBuffer, compressionMode - '0');
		switch (status)
		{
		case Z_MEM_ERROR:
			pxl::exception("pxl::oStreamer::getEvent()",
					"zlib: not enough memory");
			break;
		case Z_BUF_ERROR:
			pxl::exception("pxl::oStreamer::getEvent()",
					"zlib: buffer too small");
			break;
		case Z_STREAM_ERROR:
			pxl::exception("pxl::oStreamer::getEvent()",
					"zlib: level parameter invalid");
			break;
		default:
			break;
		}

		cZip = cZipSpace;
		lengthZip = lengthZipSpace;
	}
	else
		pxl::exception("pxl::FileChunkWriter::write()",
				"Invalid compression mode.");

	_stream.write((char *) &lengthZip, 4);
	_nBytes+=4;
	_stream.write(cZip, lengthZip);
	_nBytes+=lengthZip;

	if (cZipSpace)
		delete[] cZipSpace;

	_buffer.destroy();

	return true;
}

} //namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------




namespace pxl {

void AnalysisFork::beginJob(const pxl::ObjectOwner* input)
{
    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisFork> iter(getObjects());
       iter!=getObjects()->end(); iter++)
        (*iter)->beginJob(input);

    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisProcess> iter(getObjects());
    iter!=getObjects()->end(); iter++)
        (*iter)->beginJob(input);
}


void pxl::AnalysisFork::beginRun(const pxl::ObjectOwner* input)
{
    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisFork> iter(getObjects());
       iter!=getObjects()->end(); iter++)
        (*iter)->beginRun(input);

    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisProcess> iter(getObjects());
    iter!=getObjects()->end(); iter++)
        (*iter)->beginRun(input);
}

void pxl::AnalysisFork::analyseEvent(const pxl::ObjectOwner* input)
{
    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisFork> iter(getObjects());
       iter!=getObjects()->end(); iter++)
        (*iter)->analyseEvent(input);

    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisProcess> iter(getObjects());
    iter!=getObjects()->end(); iter++)
        (*iter)->analyseEvent(input);
}

void pxl::AnalysisFork::finishEvent(const pxl::ObjectOwner* input)
{
    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisFork> iter(getObjects());
       iter!=getObjects()->end(); iter++)
        (*iter)->finishEvent(input);

    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisProcess> iter(getObjects());
    iter!=getObjects()->end(); iter++)
        (*iter)->finishEvent(input);
}

void pxl::AnalysisFork::endRun(const pxl::ObjectOwner* input)
{
    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisFork> iter(getObjects());
       iter!=getObjects()->end(); iter++)
        (*iter)->endRun(input);

    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisProcess> iter(getObjects());
    iter!=getObjects()->end(); iter++)
        (*iter)->endRun(input);
}

void pxl::AnalysisFork::endJob(const pxl::ObjectOwner* input)
{
    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisFork> iter(getObjects());
       iter!=getObjects()->end(); iter++)
        (*iter)->endJob(input);

    for(pxl::ObjectOwner::TypeIterator<pxl::AnalysisProcess> iter(getObjects());
    iter!=getObjects()->end(); iter++)
        (*iter)->endJob(input);
}

pxl::Relative* AnalysisFork::clone() const
{
    return new AnalysisFork(*this);
}

std::ostream& AnalysisFork::print(int level, std::ostream& os, int pan) const
{
//    printPan1st(os, pan) << "pxl::CowObject<pxl::AnalysisForkData> with id: " << id() << std::endl;
//    printPan(os, pan)    << "     name: " << get().getName() << std::endl;
    printPan1st(os, pan) << "AnalysisFork: " << getName() << std::endl;
    for(pxl::ObjectOwner::const_iterator iter = getObjects()->begin();
        iter!=getObjects()->end(); ++iter) 
    {

        if ((*iter)->getMotherRelations()->size() == 0)
            (*iter)->printDecayTree(level, os, pan);
    }
    return os;
}

} // namespace pxl

//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------




namespace pxl {

pxl::Relative* AnalysisProcess::clone() const
{
    return new AnalysisProcess(*this);
}


std::ostream& AnalysisProcess::print(int level, std::ostream& os, int pan) const
{
    printPan1st(os, pan) << "AnalysisProcess: " << getName() << std::endl;
    for(pxl::ObjectOwner::const_iterator iter = getObjects()->begin();
        iter!=getObjects()->end(); iter++) {

        if ((*iter)->getMotherRelations()->size() == 0)
            (*iter)->printDecayTree(level, os, pan);
    }
    return os;
}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl
{

bool const operator==(const pxl::Basic3Vector& obj1, const pxl::Basic3Vector& obj2)
{
    return obj1.getX() == obj2.getX() && obj1.getY() == obj2.getY() && obj1.getZ() == obj2.getZ();
}

bool const operator!=(const pxl::Basic3Vector& obj1, const pxl::Basic3Vector& obj2)
{
    return obj1.getX() != obj2.getX() || obj1.getY() != obj2.getY() || obj1.getZ() != obj2.getZ();
}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl
{

bool const operator==(const pxl::Basic4Vector& obj1, const pxl::Basic4Vector& obj2)
{
    return obj1.getX() == obj2.getX() && obj1.getY() == obj2.getY() && obj1.getZ() == obj2.getZ() && obj1.getE()
            == obj2.getE();
}

bool const operator!=(const pxl::Basic4Vector& obj1, const pxl::Basic4Vector& obj2)
{
    return obj1.getX() != obj2.getX() || obj1.getY() != obj2.getY() || obj1.getZ() != obj2.getZ() || obj1.getE()
            != obj2.getE();
}

} // namespace pxl

//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------


//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl {

std::ostream& pxl::EventView::print(int level, std::ostream& os, int pan) const
{
    printPan1st(os, pan) << "EventView: " << getName() << std::endl;
    for(pxl::ObjectOwner::const_iterator iter = getObjects()->begin();
        iter!=getObjects()->end(); iter++) {

        if ((*iter)->getMotherRelations()->size() == 0)
            (*iter)->printDecayTree(level, os, pan);
    }
    return os;
}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------


std::ostream& pxl::Object::print(int level, std::ostream& os, int pan) const
{
    return printPan1st(os, pan) << "pxl::BasicObject with name: " << getName() << std::endl;
}
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl
{

bool const operator==(const pxl::Particle& obj1, const pxl::Particle& obj2)
{
	return obj1.vector() == obj2.vector() && obj1.getCharge() == obj2.getCharge();
}

bool const operator!=(const pxl::Particle& obj1,
		const pxl::Particle& obj2)
{
	return obj1.vector() != obj2.vector() || obj1.getCharge() == obj2.getCharge();
}

std::ostream& pxl::Particle::print(int level, std::ostream& os, int pan) const
{
	printPan1st(os, pan) << "Particle: '" << getName() << "', p = (" << getPt() << ", " << getPz() << ") m = " << getMass() << std::endl;
	return os;
}

} // namespace pxl

//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------

#include <cmath>


namespace pxl
{

ParticleFilter::ParticleFilter(const std::string& name, double ptMin, double etaMax) :
	_name(name), _ptMin(ptMin), _etaMax(etaMax)
{
}

bool ParticleFilter::pass(const pxl::Particle* pa) const
{
	if ((_name != "" && pa->getName() != _name) || (_ptMin > 0.0 && pa->getPt()
			< _ptMin) || (_etaMax > 0.0 && std::fabs(pa->getEta()) > _etaMax))
		return false;
	return true;
}

bool ParticleFilter::sort(const pxl::Particle* pa1, const pxl::Particle* pa2) const
{
	return ( pa1->getPt() > pa2->getPt() );
}

} // namespace pxl
//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2006-2008                  -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------



namespace pxl
{

bool const operator==(const pxl::Vertex& obj1, const pxl::Vertex& obj2)
{
	return *obj1.vector() == *obj2.vector();
}

bool const operator!=(const pxl::Vertex& obj1, const pxl::Vertex& obj2)
{
	return *obj1.vector() != *obj2.vector();
}

std::ostream& pxl::Vertex::print(int level, std::ostream& os, int pan) const
{
	printPan1st(os, pan) << "Vertex: '" << getName() << "', x = (" << getX()
			<< ", " << getY() << ", " << getZ() << ")" << std::endl;
	return os;
}

} // namespace pxl

