#ifndef PxlProcessor_h
#define PxlProcessor_h 1

#include "marlin/Processor.h"
#include "lcio.h"
#include <string>
#include "marlin/Processor.h"
#include "PxlBase.h"

using namespace lcio ;
using namespace marlin ;


/**  Example processor for marlin.
 * 
 *  If compiled with MARLIN_USE_AIDA 
 *  it creates a histogram (cloud) of the MCParticle energies.
 * 
 *  <h4>Input - Prerequisites</h4>
 *  Needs the collection of MCParticles.
 *
 *  <h4>Output</h4> 
 *  A histogram.
 * 
 * @param CollectionName Name of the MCParticle collection
 * 
 * @author F. Gaede, DESY
 * @version $Id: MyProcessor.h,v 1.4 2005/10/11 12:57:39 gaede Exp $ 
 */

class PxlProcessor : public Processor {
  
 public:
  
  virtual Processor*  newProcessor() { return new PxlProcessor ; }
  
  
  PxlProcessor() ;
  
  /** Called at the begin of the job before anything is read.
   * Use to initialize the processor, e.g. book histograms.
   */
  virtual void init() ;
  
  /** Called for every run.
   */
  virtual void processRunHeader( LCRunHeader* run ) ;
  
  /** Called for every event - the working horse.
   */
  virtual void processEvent( LCEvent * evt ) ; 
  
  
  virtual void check( LCEvent * evt ) ; 
  
  
  /** Called after data processing for clean up.
   */
  virtual void end() ;
  
  
 protected:

  /** Input collection name.
   */
  std::string _colNameMC ;
  std::string _colNameReco ;



  int _nRun ;
  int _nEvt ;

  
  // PXL output file

  pxl::OutputFile* pxlFile_;
  std::string _fileName ;

  // The labels used in cfg-file 
  std::string process_;
  
  // map to associate pdgIds with pxl names
  std::map<int, std::string> pdgMap_;


} ;

#endif



