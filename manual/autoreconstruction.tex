% Autoreconstruction paper using Template article for preprint document class `elsart'
% Last modification: Jan Steggemann 30-11-2007, Martin Erdmann 8-12-2007, JS 12-12-2007, JS 21-12-2007, JS 02-01-2008, JS 07-01-2008

\documentclass{elsart}
\usepackage{epsfig}

\usepackage{amssymb}

% \linenumbers

\begin{document}

\begin{frontmatter}

\title{An Algorithm for Automated \\ Reconstruction of 
Particle Cascades \\ in High Energy Physics Experiments}
\author{O. Actis},
\author{M. Erdmann\corauthref{cor1}},
\ead{erdmann@physik.rwth-aachen.de}
\author{A. Henrichs},
\author{A. Hinzmann},
\author{M. Kirsch},
\author{G. M\"uller},
\author{J. Steggemann}
\corauth[cor1]{Corresponding author.}
\address{III. Physikalisches Institut A, RWTH Aachen University, \\ Otto-Blumenthal-Str., D-52056 Aachen}

\begin{abstract}

We present an algorithm for reconstructing particle cascades from event data of a high energy physics experiment. For a given physics process, the algorithm reconstructs all possible configurations of the cascade from the final state objects. We describe the procedure as well as examples of physics processes of different complexity studied at hadron-hadron colliders. We estimate the performance of the algorithm by 20~$\mu$s per reconstructed decay vertex, and 0.6~kByte per reconstructed particle in the decay trees.

\end{abstract}

\begin{keyword}
High Energy Physics Algorithms \sep HEP Event Reconstruction
\PACS 29.85.-c Computer data analysis \sep 29.85.Fj Data analysis 
\end{keyword}
\end{frontmatter}

% main text
\section{Motivation}

When searching for heavy particles in high energy physics experiments, one often needs to reconstruct the whole decay chain. In these kinds of analyses, the complexity strongly increases with the depth of the cascade of the particle decay. In addition, in dependence of the number of reconstructed objects measured in the detector, the number of possible configurations of the complete cascade may become rather large. In order to avoid programming reconstruction code for every physics process individually, we present an algorithm for automated reconstruction of particle cascades.

Management of the multiple configurations together with the numerous mother-daughter relations between the particles within the decay cascade constitutes a primary challenge for the implementation of the algorithm. Here we rely on the C++ toolkit Physics eXtension Library (PXL) version 1.0.0 \cite{bib_pxl} which is a successor project of the Physics Analysis eXpert (PAX) package \cite{bib_pax}. These packages have been designed to support physicists in complex physics analyses.

The algorithm is designed to provide all possible versions of a particle cascade. It is then the task of a physicist to evaluate these multiple configurations, and to use this information for the physics interpretation of the data.

This publication is organized as follows. We first describe the algorithm technology with the steering part, the algorithm itself, and its output. Then we give several examples of physics processes, and present performance measures for each process.

\section{Algorithm}
\subsection{Steering Interface}

To steer the algorithm, a template of the cascade to be reconstructed is required. As the steering interface we use the event container provided in the PXL toolkit. The container is designed to hold all information needed in the analysis of a high energy physics collision event, e.g. particles, or data required for specific physics analyses. Particles are defined within the PXL software as well. Each particle carries a four-momentum, name, analysis-specific data, et cetera. Relations, e.g. between mother particles and daughter particles, can be established as well.

The steering event container is to be provided by a physicist. The event container is used to hold all particles which are expected in the cascade of the physics process, as well as their mother-daughter relations. This information is used by the algorithm to reconstruct the requested physics process from the objects measured in a detector.

\subsection{Event Data Interface}

To input the relevant data of a particle collision event into the algorithm, we also use the event container of the PXL software as interface. A physicist provides the corresponding event container which then holds the required reconstructed objects, e.g. muons, electrons, photons, or jets, in the form of PXL particles. In addition, the event container and its particles hold all other information needed for the physics analysis, such as missing transverse momentum, trigger information, bottom quark identification, etc.

\subsection{Reconstruction of the Particle Cascade}

The algorithm to reconstruct the particle cascade is based on an iterative procedure. In the first iteration, the steering event container is searched for two particles that do not decay further and have a common mother. In the event data, two reconstructed objects corresponding to the daughter particles are then taken to reconstruct a mother particle. Both the newly built mother and the mother-daughter relations are added to the reconstructed event data.

If more reconstructed objects of the correct type are found than required in the particle decay under consideration, multiple versions of the reconstructed event container are built. This multiplication procedure continues until all possible combinations of reconstructed objects are created to form the mother particle. 

The newly reconstructed mother particles are taken as being completed, and are then treated as non-decaying particles. Hereafter the procedure to find two particles that do not decay and have a common mother is continued until no further particle decay needs to be reconstructed.

In our C++ implementation of the algorithm as described in this paper, up to one final state particle in the steering template can be considered as having escaped detection, e.g. a neutrino. Then, the kinematics of the mother particle is calculated using the missing transverse momentum components of the event, and the mother particle mass as given in the steering event container. Depending on the solution of a quadratic equation based on a mass constraint, at most two versions of the cascade are determined. In case of an imaginary solution of the equation, only the real part is taken into account. 

\subsection{Output}

For a physics process with $n$ possible configurations of the decay cascade, the algorithm delivers $n$ event containers. Each container holds the originally reconstructed objects of the detector together with all reconstructed particles of the cascade and their corresponding mother-daughter relations.

\section{Performance}

To check the performance of the algorithm in our implementation, we use different physics processes to measure the time and memory consumption when reconstructing all possible versions of the cascade decay. For a rather complete characterization of the algorithm, we use examples of different complexity (see Fig.~\ref{fig:feyn}).

\begin{figure}
\setlength{\unitlength}{1cm}
\begin{picture}(16.0,10.0)
\put(2.0,5.6){\epsfig{file=Figure1shade.eps,width=3.7cm}}
\put(0.0,6.0){\epsfig{file=Figure1a.eps,width=5cm}}
\put(9.8,5.6){\epsfig{file=Figure1shade.eps,width=3.7cm}}
\put(8.0,6.0){\epsfig{file=Figure1b.eps,width=5cm}}
\put(1.6,-0.1){\epsfig{file=Figure1shade.eps,width=3.7cm}}
\put(0.0,0.0){\epsfig{file=Figure1c.eps,width=5cm}}
\put(10.0,-0.1){\epsfig{file=Figure1shade.eps,width=3.7cm}}
\put(8.0,0.0){\epsfig{file=Figure1d.eps,width=5cm}}
\put( 1.0,9.5){a)}
\put( 9.0,9.5){b)}
\put( 1.0,4.0){c)}
\put( 9.0,4.0){d)}
\end{picture}

\caption{Example Feynman diagrams of a) $W\ $boson production with leptonic decay, b) production of a Higgs boson decaying into four muons, c) top-antitop quark pair production with decay into a muon, neutrino, and 4 jets, d) top associated Higgs boson production with hadronic decays. The shaded areas indicate the particle cascades to be reconstructed.}
\label{fig:feyn}
\end{figure}

In our test procedure we use events produced with the MadEvent generator V4.1.33 \cite{Maltoni:2002qb} applying a setup suitable for the Large Hadron Collider (LHC). They contain the corresponding example physics process with the true particle cascade known. We then construct pseudo-data by stripping off the cascade information, and by providing only the final state particles in an event container. This event container is then passed to the algorithm as the reconstructed event data.

The histograms shown in Fig.~\ref{fig:reco} contain information from all reconstructed cascades, weighted by the inverse of the number of reconstructed event configurations. The symbols show the correctly reconstructed cascade obtained from a comparison with the true cascade.

\begin{figure}
\setlength{\unitlength}{1cm}
\begin{picture}(16.0,11.5)
\put(0.0,11.2){\epsfig{file=Figure2a.eps,width=5.5cm,angle=-90}}
\put(7.5,11.2){\epsfig{file=Figure2b.eps,width=5.5cm,angle=-90}}
\put(0.0,5.2){\epsfig{file=Figure2c.eps,width=5.5cm,angle=-90}}
\put(7.5,5.2){\epsfig{file=Figure2d.eps,width=5.5cm,angle=-90}}
\put( 1.9,10.5){a)}
\put( 9.4,10.5){b)}
\put( 1.9,4.5){c)}
\put( 9.4,4.5){d)}
\end{picture}
\caption{Correctly reconstructed event (symbols), and all reconstructed decay cascades, weighted by the inverse of the number of reconstructed event configurations (histograms). a) Difference between reconstructed and generated longitudinal momentum of the neutrino in $W\ $boson processes, b) mass of the $Z\ $bosons for processes with a Higgs boson decaying into two $Z\ $bosons, c) mass of the top quarks in top pair production processes, and d) Higgs boson mass in top associated Higgs production.}
\label{fig:reco}
\end{figure}

In Table \ref{tab_perf}, the measured performance values are shown for a single 32 bit standard personal computer. The time values give the average time per event which the computer spent on the algorithm alone, measured over at least 1000 events. The memory size provides an estimate of the maximum allocated memory of the algorithm using the difference between the memory allocation with and without our algorithm.

Estimates for the average time and memory allocation are 20~$\mu$s per vertex, and 0.6~kByte per reconstructed particle in the decay trees, respectively.
These values have been validated from extended tests where additional final state particles were added to the top quark processes such that the number of configurations per event increased.

\vspace*{0.2cm}
\begin{table}[h]
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|}
\hline
Physics	 				& number of 	& number of	& number of		& Time/event 	& Mem. alloc.     \\
process		    			& particles     & vertices	& configurations	&  [ms]      	&  [MByte]          \\

\hline
$W\rightarrow l\nu$     		&   3		&  1		&    2 			&    0.06       &  $<$ 1	 	 \\
$H\rightarrow 4\mu$			&   7		&  3		&    3 			&    0.4        &  $<$ 1		 \\
$t\bar{t}\rightarrow \mu\nu4j$   	&  11		&  5		&   24 			&    2.3       	&  $<$ 1	 	 \\
$Ht\bar{t}\rightarrow 8 j$		&  13		&  5		& 5040 			&  411       	&  36	 	       \\
\hline
\end{tabular}
\end{center}
\vspace{0.3cm}
\caption{Performance measures of the reconstruction algorithm for particle cascades (see Fig.~\ref{fig:reco}). The first column indicates the physics process. Columns 2-4 show the number of particles, vertices, and possible configurations corresponding to the steering template. The last two columns show the average time per data event and the allocated memory, respectively.}
\label{tab_perf}
\end{table}

\subsection{$W\ $Boson Decay into Leptons}

$W\ $boson production is considered as a candidate process to measure luminosity at the LHC. An example Feynman diagram is shown in Fig.~\ref{fig:feyn}a. For the steering template of the algorithm, we use the $W\ $boson, the charged lepton and the neutrino, as indicated by the shaded area in Fig.~\ref{fig:feyn}a.

In this process, typically two solutions of the kinematics of the $W\ $boson are calculated using the missing transverse momentum components and the $W\ $mass as a constraint. The resulting distribution of the difference between reconstructed and generated longitudinal momentum of the neutrino is shown in Fig.~\ref{fig:reco}a. The performance of the algorithm is 0.06~ms per event with a memory allocation of less than 1~MByte.

\subsection{Higgs Boson Decay into Four Muons}

One of the promising channels in searches for the Higgs boson predicted within the Standard Model is its decay into two $Z$ bosons, with each $Z$ decaying into two muons. An example Feynman diagram of the process is shown in Fig.~\ref{fig:feyn}b. The shaded area indicates the particles used for the steering template. Disregarding the muon charges, the cascade can be reconstructed in three different versions, each giving different reconstructed $Z$ bosons in the intermediate state. Here the algorithm needs 0.4~ms per event, with less than 1~MByte memory allocated. Fig.~\ref{fig:reco}b shows the resulting mass distribution of the $Z$ bosons.

\subsection{Top Pair Production in the Lepton Plus Jets Channel}

Owing to the large top quark mass, top quark production will be one of the most interesting Standard Model processes at the LHC. An example Feynman diagram for a top pair production process with the so-called lepton plus jets decay channel is shown in Fig.~\ref{fig:feyn}c, with the part used for the steering template indicated by the shaded area. Like in $W\ $boson production, there is a two-fold ambiguity for the reconstruction of the neutrino longitudinal momentum. Taking the quark-jet associations without identification of bottom quarks into account, there are in total 24 possible reconstruction versions. The time per event is 2.3~ms with, again, less than 1~MByte of allocated memory. The resulting top mass distribution is shown in Fig.~\ref{fig:reco}c.


\subsection{Top Quark Associated Higgs Boson Production}

Top quark associated Higgs boson production has been studied in the context of understanding the Higgs boson coupling strengths at the LHC. If all decay products of the top quarks and the Higgs boson are quarks, there are 5040 possibilities to reconstruct the event. Here we ignored identification of bottom quarks in the decay chain to test a rather extreme situation. Fig.~\ref{fig:feyn}d shows an example Feynman diagram, the shaded area indicates the part used for the steering template. Each event takes on average 411~ms to process, and a memory of 36~MByte is allocated. The Higgs mass distribution is displayed in Fig.~\ref{fig:reco}d.


\section{Summary}

In this work, we explored an automated way of reconstructing particle decay cascades. We implemented a C++ version of an algorithm which enables reconstruction of all possible configurations of a decay chain from the final state objects measured in a detector. The algorithm performs according to a single template that represents the requested decay chain and is provided by the user. We validated the algorithm using several physics processes of different complexity, and verified that the performance values with respect to time and memory consumption are within the scope of a typical physics analysis in high energy physics experiments.

\section*{Acknowledgements}

We are very grateful for financial support of the Ministerium f\"{u}r Innovation, Wissenschaft, Forschung und Technologie des Landes
Nordrhein-Westfalen, the Bundesministerium f\"{u}r Bildung und Forschung (BMBF), and the Deutsche Forschungsgemeinschaft (DFG).


\begin{thebibliography}{00}


\bibitem{bib_pxl}
M.~Erdmann, S.~Kappler, M.~Kirsch, G.~M\"uller, C.~Saout, J.~Steggemann,
Physics eXtension Library, http://cern.ch/pax/pxl.


\begin{subbibitems}{bib_pax}
\bibitem{Erdmann:2002wn}
  M.~Erdmann, D.~Hirschb\"{u}hl, Y.~Kemp, P.~Schemitz, T.~Walter,
  ``Physics analysis expert,''
%\href{http://www.slac.stanford.edu/spires/find/hep/www?irn=5764939}{SPIRES entry}
  Proc. of the 14th Topical Conference on Hadron Collider Physics (HCP 2002), Karlsruhe, Germany, (2002), publ. by Springer Verlag, Berlin, Germany, (2003).

%\cite{Erdmann:2003ke}
\bibitem{Erdmann:2003ke}
  M.~Erdmann, D.~Hirschb\"{u}hl, C.~Jung, S.~Kappler, Y.~Kemp, M.~Kirsch,
  ``Physics analysis expert PAX: First applications,''
  Proc. of 2003 Conference for Computing in High-Energy and Nuclear Physics (CHEP 03), La Jolla, California, (2003), [arXiv:physics/0306085].

\bibitem{chep04}
  M.~Erdmann, U.~Felzmann, D.~Hirschb\"uhl, C.~Jung, S.~Kappler, M.~Kirsch, G.~Quast, K.~Rabbertz, J.~Rehn, S.~Schalla, P.~Schemitz, A.~Schmidt, T.~Walter, C.~Weiser,
  ``New Applications of PAX in Physics Analyses at Hadron Colliders,''
  Proc. of the Conference on Computing in High Energy and Nuclear Physics (CHEP04), Interlaken, Switzerland, (2004).

%\cite{Kappler:2005tf}
\bibitem{Kappler:2005tf}
  S.~Kappler, M.~Erdmann, U.~Felzmann, D.~Hirschb\"uhl, M.~Kirsch, G.~Quast, A.~Schmidt, J.~Weng,
  ``The PAX toolkit and its applications at Tevatron and LHC,''
  IEEE Trans.\ Nucl.\ Sci.\  {\bf 53}, 506 (2006),
  [arXiv:physics/0512232].
  %%CITATION = IETNA,53,506;%%

%\cite{Kappler:2006uq}
\bibitem{Kappler:2006uq}
  S.~Kappler, M.~Erdmann, M.~Kirsch, G.~M\"uller, J.~Weng, A.~Flossdorf, U.~Felzmann, G.~Quast, C.~Saout, A.~Schmidt,
  ``Concepts, developments and advanced applications of the PAX toolkit,''
 Proc. of the Conference on Computing in High Energy and Nuclear Physics (CHEP06), Mumbai, India, (2006),
  [arXiv:physics/0605063].
  %%CITATION = PHYSICS/0605063;%%
\end{subbibitems}

%\cite{Maltoni:2002qb}
\bibitem{Maltoni:2002qb}
  F.~Maltoni and T.~Stelzer,
  ``MadEvent: Automatic event generation with MadGraph,''
  JHEP {\bf 0302}, 027 (2003)
  [arXiv:hep-ph/0208156].
  %%CITATION = JHEPA,0302,027;%%

\end{thebibliography}

\end{document}

