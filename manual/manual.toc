\contentsline {section}{\numberline {1}\relax \fontsize {14.4}{18}\selectfont Overview}{3}{section.1}
\contentsline {section}{\numberline {2}Concept and Structure of PXL}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Particle, Vertex, Collision: Physics Objects}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}User Records}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Relation Management}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Event, Event View: Object Owner}{9}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Pointer and Weak Pointer}{13}{subsection.2.5}
\contentsline {section}{\numberline {3}PXL I/O}{14}{section.3}
\contentsline {section}{\numberline {4}Additional Options}{18}{section.4}
\contentsline {section}{\numberline {5}Technical details}{19}{section.5}
\contentsline {subsection}{\numberline {5.1}PXL Classes}{19}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Copy-On-Write Concept}{20}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}UUID}{21}{subsection.5.3}
\contentsline {section}{\numberline {6} PXL download and installation}{21}{section.6}
