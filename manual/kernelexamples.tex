\section{\large Examples of PAX kernel functionalities \label{sec:examples}}
As mentioned at the end of section~\ref{sec:installation} we deliver example programs together with the kernel, in order to guide you through your first experiences using PAX.
You find them in the directory {\bf\red PAX/test/} of your PAX repository.
The files {\bf\red paxdemo.cc} and {\bf\red paxsimpleanalysis.cc} contain the source code of the examples.\\
If you haven't done it during the installation of the PAX kernel, please change to this directory and compile the examples.\\
{\bf\red $>$ cd PAX/test}\\
{\bf\red $>$ gmake}\\
\noindent
After the compilation you will find the two executables {\bf\red paxdemo} and {\bf\red paxsimpleanalysis} in your directory.\\

\noindent
In this section we will show you some of the most common applications of the PAX kernel functionalities, which should serve you as a starting point for the development of your own analysis.\\
For the explanations in the different subsections we will switch between the two examples.
If source code is shown, the original example file will be indicated in {\bf\blue blue}.\\

\noindent
Some introductory remarks on the two examples:
\begin{itemize}
\item {\bf\red paxdemo.cc}:\\
This example provides an overview about the basic kernel functionalities.
The source code is commented in detail.
Since the example goes beyond the documentation in this Users Guide, please read the source code for more information.
\item {\bf\red paxsimpleanalysis.cc}\\
The example {\bf\red paxsimpleanalysis.cc} contains the whole analysis chain that one would find in a real experiment environment.
In the class {\bf\red ZDiceExperiment} the output of the reconstruction software of a detector is simulated.
Thus, this class is foreign to PAX.
{\bf\red ZDiceExperiment} owns a member function {\bf\red reconstructEvent(int iEvent)}.
In this function Z bosons decaying into two muons are diced with the assumption, that the Z is produced almost at rest.
The kinematics of the muons are calculated and smeared with a gaussian distribution.\\
The interface between the experiment specific class {\bf\red ZDiceExperiment} and the PAX toolkit is defined in the class {\bf\red PaxZDiceExperimentFiller}.
This class illustrates for this special case, how an interface between a data summary of an experiment and the PAX toolkit could look like.
Finally, in the class {\bf\red PaxSimpleZAnalysis} the analysis of the reconstructed particles is done using PAX objects.
The combination of the muons to a Z boson is done in the member function {\bf\red analyzeEvent}.
\end{itemize}

\noindent
Now, run the examples.
It could be useful to store the output in a logfile.\\
{\em Additional comments to the content of the logfiles are marked in this format}\\
{\bf\red $>$ ./paxdemo $>$ paxdemo.log}\\
The example {\bf\red paxsimpleanalysis} must be called with a command line option.
Type\\
{\bf\red $>$ ./paxsimpleanalysis $--$help}\\
to get a brief explanation of the three options you can choose.
Run all three of them to get an impression about the time it takes to analyze these 10000 events.

\subsection{\large Book, fill and print event interpreations }\label{ssec:bookei}
An instance of the class {\bf\red PaxEventInterpret} can be booked by using its default constructor ({\blue paxdemo.cc}):\\

\noindent
// book new event interpretation\\
// =============================\\
\bredb{  PaxEventInterpret* eventinterpret   = new PaxEventInterpret;}\\
// name the event interpretation \\
\bredb{  eventinterpret-$>$setPaxName("test event interpretation");}\\
\bredb{$\dots$}

\noindent
Another possibility to book a new event interpretation is to use the copy constructor and create a deep copy of another event interpretation (e.g. to test different hypotheses on them) ({\blue paxdemo.cc}).\\

\noindent
// create a copy:\\
\bredb{PaxEventInterpret* eventinterpretPrime = new PaxEventInterpret(eventinterpret);}
\bredb{eventinterpretPrime-$>$setPaxName("test event interpretation copy");}\\
\bredb{$\dots$}\\

\noindent
The easiest way of getting an event interpretation is a copy of the reconstruction output of the experiment.
This is done in the class member function \bredb{fillMuons} of the class \bredb{PaxZDiceFiller} of the example {\blue paxsimpleanalysis}.
In the analysis the following commands are called:\\

\noindent
// create a new event container:\\
\bredb{PaxEventInterpret* eiRecParticles = new PaxEventInterpret;}\\
\bredb{eiRecParticles-$>$setPaxName("Reconstructed Particles");}\\
// fill the reconstructed objects:\\
\bredb{filler-$>$fillMuons(eiRecParticles);}\\
\bredb{$\dots$}\\
\noindent
In the method \bredb{fillMuons} the fourvectors of the reconstruction are filled into \bredb{PaxFourVector}s with name ''muon'', charge $\pm 1$ and particle ID $\pm 13$.
All these objects are registered in the event interpretation ''Reconstructed Particles''.\\

\noindent
The content of an event interpretation (i.e. the physics objects and their relations) can be printed out ({\blue paxdemo.cc}):\\

\noindent
\bredb{eventinterpret-$>$print();}\\

\noindent
The printout then looks like ({\blue paxdemo.log}):\\
*****************************************************************************************************************\\
*  PaxEventInterpret::print\\
*  PaxId = 1 {\em (pax objects have a unique identifier PaxId)}   name = test event interpretation   workflag = 0\\
*****************************************************************************************************************\\

\noindent 
*********************************\\
PaxEventInterpret::print: Collisions\\
*********************************\\
number of collisions= 1\\
==========================================================================\\
PaxId=2 name =hard collision status =0 workflag =0 locked =0\\
...................................................................\\
PaxCollisionRelations::print: total number of members=1\\
PaxId=2 name =hard collision status =0 workflag =0 locked =0\\
...................................................................\\
PaxVertexRelations::print: total number of members=1\\
PaxId=3 name =primary vertex x=0.23 y=0.15 z=12.7 status =0 workflag =0 locked =0\\
...................................................................\\
ExperimentClassRelationsMap: PaxExperimentClassRelations name= class ExpSpecReco\\
PaxExperimentClassRelations::print:  total number of relations=1\\
related experimentclass key=-1\\
----------------------------------\\

\noindent 
*******************************\\
PaxEventInterpret::print: Vertices   \\
*******************************\\
number of vertices= 1\\
=======================================================================\\
PaxId=3 name =primary vertex x=0.23 y=0.15 z=12.7 status =0 workflag =0 locked =0\\
...................................................................\\
PaxCollisionRelations::print: total number of members=1\\
PaxId=2 name =hard collision status =0 workflag =0 locked =0\\
...................................................................\\
PaxVertexRelations::print: total number of members=1 {\em (required for analysis history and the exclusion mechanism)}\\
PaxId=3 name =primary vertex x=0.23 y=0.15 z=12.7 status =0 workflag =0 locked =0\\
...................................................................\\
PaxOutgoingFourVectorRelations::print:  total number of fourvectors=2 {\em (required for decay chains)}\\
PaxId=5 name =electron px=-2.6 py=-5.4 pz=1.3 e=6.1327 charge=-1 mass=0.000511 id=0 status=0 simulated=0 workflag=0 locked=0\\
PaxId=4 name =pion px=3.2 py=5.1 pz=1.45 e=6.1945 charge=1 mass=0.139 id=0 status=0 simulated=0 workflag=0 locked=0\\
...................................................................\\
ExperimentClassRelationsMap: PaxExperimentClassRelations name= class ExpSpecReco\\
PaxExperimentClassRelations::print:  total number of relations=1\\
related experimentclass key=0\\
----------------------------------\\
 
\noindent
**********************************\\
PaxEventInterpret::print: FourVectors\\
**********************************\\
number of fourvectors= 2\\
==========================================================================\\
PaxId=5 name =electron px=-2.6 py=-5.4 pz=1.3 e=6.1327 charge=-1 mass=0.000511 id=0 status=0 simulated=0 workflag=0 locked=0\\
...................................................................\\
PaxBeginVertexRelations::print: total number of members=1 {\em (required for decay chains)}\\
PaxId=3 name =primary vertex x=0.23 y=0.15 z=12.7 status =0 workflag =0 locked =0\\
...................................................................\\
PaxFourVectorRelations::print:  total number of fourvectors=1 {\em (required for analysis history and the exclusion mechanism)}\\
PaxId=5 name =electron px=-2.6 py=-5.4 pz=1.3 e=6.1327 charge=-1 mass=0.000511 id=0 status=0 simulated=0 workflag=0 locked=0\\
...................................................................\\
ExperimentClassRelationsMap: PaxExperimentClassRelations name= class ExpSpecReco\\
{\em (enables acces to the original reconstruction class member functions)}\\
PaxExperimentClassRelations::print:  total number of relations=1\\
related experimentclass key=2\\
...................................................................\\
PaxUserRecord::print: total number of user records= 1\\
name=number of tracker hits value=58\\
==========================================================================\\
(...)\\
*********************************\\
PaxEventInterpret::print: PaxUserRecord\\
*********************************\\
...................................................................\\
PaxUserRecord::print: total number of user records= 5\\
name=trigger number value=5.4679e+06\\
name=id\_primary vertex value=3\\
(...)\\

\noindent
If you prefer the kinematics in the form of (pt,eta,phi) instead of (px,py,pz) use the PAX printlevels (section~\ref{printlevel}).\\
\bredb{eventinterpret-$>$print(PAX\_PT);}\\
An example for this can be found in \blfi{paxdemo.log}.\\
If the output is too elaborate for your taste, try\\
\bredb{eventinterpret-$>$print(PAX\_PTSHORT);}\\


\subsection{\large Book PAX objects in event interpretations}\label{ssec:bookobj}
Of course the objects \bredb{PaxCollision}, \bredb{PaxVertex} and \bredb{PaxFourVector} can be instantiated as any other object in C++ by using the \bredb{new} command (e.g. a \bredb{PaxVertex} in \blfi{paxdemo.cc}):\\
// and we have a vertex:\\
\bredb{PaxVertex* vertexXY = new PaxVertex;}\\

\noindent
Analogously this can be done for the other objects:\\
\bredb{PaxFourVector* fourvector = new PaxFourVector;}\\
\bredb{PaxFourCollision* collision = new PaxCollision;}\\
or by using their copy constructors.\\
In this case it is the user's task to delete these objects to avoid memory leaks.

\noindent
If a PAX object is used together with an event interpretation, it is useful to register the object directly with the event interpretation by using the \bredb{create} method.
In this case, the objects are delete, when the destructor of the event interpret ist called.
The user does not have to care about the deletion (\blfi{paxdemo.cc}):\\

\noindent
// book new collision within this event interpretation\\
\bredb{PaxCollision* collision = eventinterpret-$>$create$<$PaxCollision$>$();}\\
// name the collision\\
\bredb{collision-$>$setPaxName("hard collision");}\\
\bredb{$\dots$}\\

\noindent
// analogously: book new vertex within this event interpretation\\
// =============================================================\\
\bredb{PaxVertex* vertex = eventinterpret-$>$create$<$PaxVertex$>$();}\\
\bredb{vertex-$>$setPaxName("primary vertex");}\\
\bredb{vertex-$>$setX(0.23);}\\
\bredb{vertex-$>$setY(0.15);}\\
\bredb{vertex-$>$setZ(12.7);}\\
\bredb{$\dots$}\\

\noindent
// analogously: book new fourvectors within this event interpretation\\
// =============================================================\\
\bredb{PaxFourVector* fourvector1 = eventinterpret-$>$create$<$PaxFourVector$>$();}\\
\bredb{fourvector1-$>$setPaxName("pion");}\\
\bredb{fourvector1-$>$setPx(3.2);}\\
\bredb{fourvector1-$>$setPy(5.1);}\\
\bredb{fourvector1-$>$setPz(1.45);}\\
\bredb{fourvector1-$>$setMass(0.139);}\\
\bredb{fourvector1-$>$setCharge(1.);}\\
\bredb{$\dots$}\\

\noindent
After you have analyzed the event and stored the results, you delete the eventinterpretation and at the same time all physics objects that are registered with it (\blfi{paxdemo.log}):\\

\noindent
\bredb{delete eventinterpret;}      // (deletes also all physics objects inside)\\


\subsection{\large Loop over PaxFourVectors and histogram their transverse momenta}
If the ROOT option was chosen during the compilation of the PAX kernel (compare section~\ref{sec:installation}) the example \bredb{paxsimpleanalysis} produces both a file \blfi{pasimpleanalysis.root} and \blfi{paxsimpleanalysis.eps} where some histograms are stored either in form of ROOT objects or in postscript format.
The definition of these histogram is done in the constructor of \bredb{PaxSimpleZAnalysis}:\\

\noindent
\bblueb{\#ifdef} PAX\_ROOT\\
\bredb{  th1fMuPt = new TH1F("Muon: Pt","Muon: Pt", 20,   0,100);}\\
\bredb{$\dots$}\\
\bblueb{\#endif  }\\

\noindent
In order to find the leading muon and anti-muon in the muon sample (the event may contain more than two muons), we loop over all the fourvectors (\blfi{paxsimpleanalysis.cc}):\\

\noindent
\bredb{PaxIterator$<$PaxFourVector*$>$* iter = ei-$>$getFourVectors()-$>$getIterator();}\\
\bredb{iterate (iter) \{}\\
\bredb{   PaxFourVector* fv = iter-$>$CurrentItem();}\\
\bredb{   // look for the muon (charge $\sim$ -1)}\\
\bredb{   if (fv-$>$getPaxName() == "muon" \&\& fv-$>$getCharge() $<$ 0.)  \{}\\
\bredb{      if (fv-$>$Perp() $>$ muPt)     \{muPt = fv-$>$Perp();    mu = fv;\}}\\
\bredb{      \}}\\
\bredb{   // look for the anti-muon (charge $\sim$ +1) }\\
\bredb{   if (fv-$>$getPaxName() == "muon" \&\& fv-$>$getCharge() $>$ 0.)  \{ }\\
\bredb{      if (fv-$>$Perp() $>$ mubarPt)  \{mubarPt = fv-$>$Perp(); mubar = fv;\} }\\
\bredb{      \}}\\
\bredb{  \}}\\

\noindent
Please note, that the command\\
\bredb{iterate (iter)\{$\dots$\}}\\
is a short-cut for iterating maps of fourvectors, vertices, etc, (\blfi{paxsimpleanalysis.cc}):\\
//----------------------------------------------------------------------\\
// Define a short-cut for iterating maps of fourvectors, vertices, etc.\\
// (valid within this .cc file):\\
\bblueb{\#define} \bredb{iterate(iter)   for (iter-$>$First();!iter-$>$IsDone();iter-$>$Next())}\\

\noindent
Now, you can fill the transverse momenta of the muons in the booked histograms:\\

\noindent
\bblueb{\#ifdef} PAX\_ROOT\\
\bredb{// fill histograms:}\\
\bredb{th1fMuPt-$>$Fill( mu-$>$Perp() );}\\
\bredb{th1fMuPt-$>$Fill( mubar-$>$Perp() );}\\
\bblueb{\#endif}\\

\noindent
And finally these histograms are written to a ROOT file:\\

\noindent
\bblueb{\#ifdef} PAX\_ROOT\\
//save histograms to root file\\
\bredb{TFile f("paxsimpleanalysis.root", "recreate");}\\
\bredb{th1fMuPt-$>$Write();}\\
\bredb{$\dots$}\\
\bredb{f.Close();}\\

\noindent
// save histograms to eps file:\\
\bredb{TCanvas cv;}\\
\bredb{cv.Divide(3,3);}\\
\bredb{cv.cd(2); th1fMuPt-$>$Draw();}\\
\bredb{$\dots$}\\
\bredb{cv.Print("paxsimpleanalysis.eps");}\\

\noindent
// clean up:  \\
\bredb{delete th1fMuPt;}\\
\bblueb{\#endif}


\subsection{\large Loop over PaxVertex related PaxFourVectors }
Find the primary vertex and print the names and pseudo-rapidities of the fourvectors originating from it (\blfi{paxdemo.cc}):\\

\noindent
// get iterator for vertices from event interpret \\
\bredb{PaxIterator$<$PaxVertex*$>$* iter = eventinterpret-$>$getVertices()-$>$getIterator();}\\
\bredb{for (iter-$>$First(); !iter-$>$IsDone(); iter-$>$Next() )}\\
\bredb{  \{}\\
\bredb{       cout  $<<$ "  name = " $<<$ iter-$>$CurrentItem()-$>$getPaxName() }\\
    // get iterator for fourvectors via the outgoing fourvector relations\\
   // of the vertices of event interpret\\
\bredb{    PaxIterator$<$PaxFourVector*$>$* subiter = iter-$>$CurrentItem()}\\
\bredb{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-$>$getOutgoingFourVectorRelations()}\\
\bredb{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-$>$getIterator();}\\
\bredb{    for (subiter-$>$First(); !subiter-$>$IsDone(); subiter-$>$Next() )}\\
\bredb{      \{}\\
\bredb{        cout $<<$ "pseudo-rapidity = $<<$ subiter-$>$CurrentItem()-$>$eta() }\\
\bredb{      	$<<$ "       name = " $<<$ subiter-$>$CurrentItem()-$>$getPaxName()}\\
\bredb{      	$<<$ endl;}\\
\bredb{      \}}\\
\bredb{  \}}\\


\subsection{\large Access original member functions of the experiments' reconstruction}
\noindent
Information needed from the  reconstruction output is best accessed within the interface functions to the experiment
data (e.g. fillMuons, see section~\ref{ssec:bookei}).
There the best knowledge should be passed to the event interpretations of PAX in terms of vertices, fourvectors, etc.
Nevertheless there is a way to recover the original pointer to the instance of a CMS detector class and access its member function.\\
In the example \blfi{paxdemo.cc} an experiment specific reconstruction class \bredb{ExpSpecReco} is defined, which is foreign to PAX.\\

\noindent
// provide pointers to reconstruction \\
// class instances of physics objects\\
// ==================================\\
\noindent
// let's assume ExpSpecReco to be an experiment-specific class,\\
// and we have the following instances:\\
\bredb{ExpSpecReco* originalco = new ExpSpecReco(-1);}\\
\bredb{ExpSpecReco* originalvx = new ExpSpecReco(0);}\\
\bredb{ExpSpecReco* originalfv1 = new ExpSpecReco(1);}\\
\bredb{ExpSpecReco* originalfv2 = new ExpSpecReco(2);}\\

\noindent
// provide a pointer to the original experiment \\
// reconstruction class of the two fourvectors\\
// registered as "class ExpSpecReco" and in the \\
// slots number 1 and 2, respectively\\
\bredb{fourvector1-$>$addPointer$<$ExpSpecReco$>$("class ExpSpecReco", 1, originalfv1 );}\\
\bredb{fourvector2-$>$addPointer$<$ExpSpecReco$>$("class ExpSpecReco", 2, originalfv2 );}\\
\noindent
// the same for vertex...\\
\bredb{vertex-$>$addPointer$<$ExpSpecReco$>$("class ExpSpecReco", 0, originalvx );}\\
\noindent
// ...and collision.\\
\bredb{collision-$>$addPointer$<$ExpSpecReco$>$("class ExpSpecReco", -1, originalco );}\\

\noindent
Now, we simulate the situation, the we have fogotten the original pointers:\\

\noindent
// we forget the original pointers:\\
\bredb{originalfv1 = 0;}\\
// and retrieve them by\\
\bredb{originalfv1 = fourvector1-$>$findPointer$<$ExpSpecReco$>$("class ExpSpecReco", 1);}\\
// access the object, if found:\\
\bredb{if (originalfv1) originalfv1-$>$print();}\\
// the same, even shorter (but may crash!), for vertex...\\
\bredb{vertex-$>$findPointer$<$ExpSpecReco$>$("class ExpSpecReco", 0)-$>$print();}\\
// ...and collision.\\
\bredb{collision-$>$findPointer$<$ExpSpecReco$>$("class ExpSpecReco", -1)-$>$print();}\\

\subsection{\large Reconstruct Z bosons in leptonic decays}
The example algorithm \bredb{PaxSimpleZAnalysis::analyzeEvent} in \blfi{paxsimpleanalysis.cc} reconstructs the decay chain Z -$>$ 2 muons.
Its input is an event interpretation containing all muons from the reconstruction (which is simulated by dicing their fourvectors in \bredb{ZDiceExperiment::reconstructEvent}).
On output it delivers an event interpretation containing the one solution chosen for the reconstruction of the Z boson (only one solution, since the (anti-)muon with the highest transverse momentum is selected).\\

\noindent
// copy the original event interpretation for this hypothesis:\\
\bredb{PaxEventInterpret* ei = new PaxEventInterpret(eiParticles);}\\
\bredb{$\dots$}\\

\noindent  
// if muon \& anti-muon were found, reconstruct the Z boson:\\
\bredb{if (mu \&\& mubar) \{ }\\
\bredb{     if (mu-$>$Perp() $>$ 15. \&\& mubar-$>$Perp() $>$ 15.) \{}\\
	// create a decay vertex for the Z:\\
\bredb{	PaxVertex* vertex = ei-$>$create$<$PaxVertex$>$();}\\
	// connect it to the muons\\
\bredb{	mu-$>$getBeginVertexRelations()-$>$add(vertex, mu);}\\
\bredb{	mubar-$>$getBeginVertexRelations()-$>$add(vertex, mubar);}\\
	// Make a mother particle at the vertex: the Z\\
	// (Please notice: the fourmomentum will be \\
	//  calculated within the following statement)\\
\bredb{	PaxFourVector* Z = ei-$>$create$<$PaxFourVector, PaxVertex$>$(vertex);}\\
\bredb{	Z-$>$setPaxName("Z");}\\
\bredb{	Z-$>$setParticleId(23);}\\
\bredb{$\dots$}\\
\bredb{\}}\\
\bredb{$\dots$}\\
// clean up:\\
\bredb{delete ei;}\\

\noindent
The decay tree of the Z boson can be printed out:\\
\noindent
// print decay tree\\
\bredb{Z-$>$printDecayTree();}\\

\noindent
The output of this command looks like (\blfi{paxsimpleanalysis.log}):\\

\noindent
Z  PaxId=11 pdg=23 pt=0.616777 eta=-0.80654 phi=1.04623\\
\{default\}  PaxId=10 x=0 y=0 z=0\\
$|$\_\_muon  PaxId=9 pdg=13 pt=59.6219 eta=0.292216 phi=-1.46446\\
$|$\_\_muon  PaxId=8 pdg=-13 pt=60.121 eta=-0.298672 phi=1.67108\\




\subsection{\large Write event interpretations to file, read from file }
To store event interpretations to disk PAX provides the class \bredb{PaxIoFile}.
In example \blfi{paxdemo.cc} it is shown, how to write and read events.\\

\noindent
// writing a file (3 times the same 2 event interpretations)\\
\bredb{\{}\\
{\em open a new PaxIoFile for writing}\\
\bredb{ PaxIoFile* iofile = new PaxIoFile("paxdemo.paxio", PaxIoFile::Write);}\\
\bredb{ for (int i=0; i$<$3; i++) \{ }\\
     // one may want to isolate copied event interpretations\\
     //eventinterpretPrime-$>$isolate();\\
     // (this cuts relations to physics objects\\
     // outside of the event interpretation itself)\\
     
\noindent
     // store the event interprets to the event buffer\\
\bredb{     iofile-$>$store(eventinterpret);}\\
\bredb{     iofile-$>$store(eventinterpretPrime);}\\
     
     // write the event buffer physically to disk:\\
\bredb{     iofile-$>$writeEvent();}\\
\bredb{     \} }\\
\bredb{ delete iofile;} // file is also closed\\
\bredb{ \} }\\

\noindent
*************************************************\\
\bredb{\{}\\
// reading a file, skipping the first two events:\\
\bredb{ PaxIoFile* iofile = new PaxIoFile("paxdemo.paxio", PaxIoFile::Read);}\\
{\em While reading a PaxIoFile you can skip events using the skipEvents() method}\\
\bredb{ iofile-$>$skipEvents(2);}\\
\bredb{ while ( !iofile-$>$endOfFile() ) \{ }\\
      // read the event:\\
\bredb{      iofile-$>$readEvent();}\\
      // this method allows to read complete events with external\\
      // relations as they appear when copying event interpretations.\\
{\em If you have chosen to store isolated event interpretations, \\you can use the following command to gain perfomance}\\
      //\bredb{iofile-$>$readEventIsolated();}\\
\bredb{      while ( !iofile-$>$endOfEvent() ) \{}\\
\bredb{         PaxEventInterpret* eventinterpretFromFile = new PaxEventInterpret;}\\
	 
	 // restore the event interpreation\\
\bredb{	 iofile-$>$restore(eventinterpretFromFile);}\\
\bredb{	 delete eventinterpretFromFile;}\\
\bredb{         \}}\\
\bredb{      \}}\\
\bredb{   delete iofile;} // file is also closed\\
\bredb{\}}\\

\noindent
The usage of the \bredb{PaxIoFile} is also shown in the example \blfi{paxsimpleanalysis}.
Try the options 'tofile' and 'fromfile' when calling it.
The option 'tofile' shows, how to create \bredb{PaxIoFile}s for the output of the experiments' reconstruction software.
Storing all necessary information from the reconstruction into PAX objects (you can use the \bredb{PaxUserRecord} to do so) and writing them to a file, your analysis becomes independant of your experiment software.
