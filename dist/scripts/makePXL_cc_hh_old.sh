#!/bin/sh
# merge pxl into ONE cc and ONE hh.
# execute from PXL basedir.

if [ -n "$RELEASE_TOOLS" ]; then
	svn export --username "$SVN_USER" --password "$SVN_PASSWORD" \
		"$SVN_URL/$RELEASE_TOOLS/trunk/merge.py" || return 1
	BASE=.
else
	BASE=`dirname $0`
fi

PXL_MAJOR_VERSION=`cat configure.in | grep "PXL_MAJOR_VERSION="`
PXL_MINOR_VERSION=`cat configure.in | grep "PXL_MINOR_VERSION="`
PXL_EXTRA_VERSION=`cat configure.in | grep "PXL_EXTRA_VERSION="`

PXL_MAJOR_VERSION=`echo ${PXL_MAJOR_VERSION#P*=}`
PXL_MINOR_VERSION=`echo ${PXL_MINOR_VERSION#P*=}`
PXL_EXTRA_VERSION=`echo ${PXL_EXTRA_VERSION#P*=}`

echo "//-------------------------------------------" > PXL.hh
echo -n "//  PXL header bundle, version " >> PXL.hh
echo -n $PXL_MAJOR_VERSION >> PXL.hh
echo -n  "." >> PXL.hh
echo -n  $PXL_MINOR_VERSION >> PXL.hh
echo -n  "." >> PXL.hh
echo -n  $PXL_EXTRA_VERSION >> PXL.hh
echo "        -" >> PXL.hh
echo -n "//  Created on " >> PXL.hh
echo -n `date` >> PXL.hh
echo " -" >> PXL.hh
echo "//-------------------------------------------" >> PXL.hh

echo "//-------------------------------------------" > PXL.cc
echo -n "//  PXL source bundle, version " >> PXL.cc
echo -n $PXL_MAJOR_VERSION >> PXL.cc
echo -n  "." >> PXL.cc
echo -n  $PXL_MINOR_VERSION >> PXL.cc
echo -n  "." >> PXL.cc
echo -n  $PXL_EXTRA_VERSION >> PXL.cc
echo "        -" >> PXL.cc
echo -n "//  Created on " >> PXL.cc
echo -n `date` >> PXL.cc
echo " -" >> PXL.cc
echo "//-------------------------------------------" >> PXL.cc

python $BASE/merge.py -frR -I pxl/include -I hep/include -I astro/include pxl/include/pxl/pxl.hh >> PXL.hh

python $BASE/merge.py -rR -e PXL.hh -I pxl/include -I hep/include -I astro/include pxl/src/base/*.cc pxl/src/io/*.cc hep/src/*.cc astro/src/*.cc >> PXL.cc

if [ -n "$RELEASE_TOOLS" ]; then
	zip -9 ../PXL.zip PXL.{cc,hh}
fi
