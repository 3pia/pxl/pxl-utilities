#!/usr/bin/env bash

# merge pxl into one *.cc and one *.h file
# execute in the pxl base directory

BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# define the target files
HEADER_FILE=Pxl.h
SOURCE_FILE=Pxl.cc

# delete already present files
rm -f $HEADER_FILE
rm -f $SOURCE_FILE

# create the merged header file
python $BASE/merge.py -frR -I build/include -I core/include -I hep/include core/include/pxl/core.hh hep/include/pxl/hep.hh > $HEADER_FILE

# create the merged source file
python $BASE/merge.py -rR -e $HEADER_FILE -I core/include -I hep/include core/src/*.cc hep/src/*.cc > $SOURCE_FILE
