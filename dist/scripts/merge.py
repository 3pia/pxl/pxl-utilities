#!/usr/bin/env python
import sys, os, getopt
from sets import Set

# global defines
ST_UNKNOWN	= 1
ST_SKIPPED	= 2
ST_INCLUDED	= 3

class Merger:
	# Define statement registry and decorator
	class Statement:
		def __init__(self, statements):
			self.statements = statements

		def __call__(self, f):
			name = f.__name__
			if name.endswith('_'):
				name = name[:-1]
			self.statements[name] = f
			return f

	__statements = {}
	statement = Statement(__statements)

	# Initialization of Merger class
	def __init__(self, outFile, flags, includeFn):
		self.out = outFile
		self.flags = flags
		self.includeFn = includeFn
		self.headers = Set()
		self.rawBuffer = ""
		self.cookedBuffer = ""
		self.defines = {}
		self.states = [ ST_INCLUDED ]
		self.exchangeHeader = None

	def setExchange(self, header):
		self.exchangeHeader = header

	def addDefine(self, key, value):
		self.defines[key] = value

	def addUndef(self, key):
		self.defines[key] = ST_SKIPPED

	# Process one input line
	def process(self, line):
		self.rawBuffer += line
		line = line.strip()
		if line != '' and line.endswith('\\'):
			self.cookedBuffer += line[:-1]
			return
		else:
			self.cookedBuffer += line

		origLine = self.rawBuffer
		line = self.cookedBuffer
		self.rawBuffer = ""
		self.cookedBuffer = ""

		if line == '' or line[0] != '#':
			self.emit(origLine)
			return

		tokens = line[1:].split()
		try:
			handler = Merger.__statements[tokens[0]]
		except:
			self.emit(origLine)
			return

		if not handler(self, tokens[1:]):
			self.emit(origLine)

	def emit(self, line):
		if self.states[-1] != ST_SKIPPED:
			self.out.write(line)

	# Start preprocessor statement handlers
	@statement
	def include(self, tokens):
		if len(tokens) != 1 or self.states[-1] == ST_SKIPPED:
			return False
		header = tokens[0]

		if header[0] == '<' and header[-1] == '>':
			sysHeader = True
		elif header[0] == '"' and header[-1] == '"':
			sysHeader = False
		else:
			return False

		header = header[1:-1]

		replace = not sysHeader and self.exchangeHeader
		if replace:
			header = self.exchangeHeader

		if "rR"[sysHeader] in self.flags and header in self.headers:
			return True
#		elif self.states[-1] == ST_INCLUDED:
		else:
			self.headers.add(header)

		if "fF"[sysHeader] in self.flags:
			return self.includeFn(header)

		if replace:
			self.emit('#include "%s"\n' % header)
			return True
		else:
			return False

	@statement
	def define(self, tokens, undef = False):
		if len(tokens) < 1:
			return False

		define = tokens[0]
		if undef:
			value = ST_SKIPPED
		elif len(tokens) == 1:
			value = ""
		else:
			value = reduce(lambda x, y: x + ' ' + y, tokens[1:])

		try:
			old = self.defines[define]
		except:
			old = ST_UNKNOWN

		state = self.states[-1]
		if state == ST_UNKNOWN:
			if old == value:
				pass
			elif not old in (ST_UNKNOWN, ST_SKIPPED) \
			   and not value in (ST_UNKNOWN, ST_SKIPPED):
				self.defines[define] = ST_INCLUDED
			elif old != ST_UNKNOWN:
				del(self.defines[define])

		elif state == ST_INCLUDED:
			self.defines[define] = value

		return False

	@statement
	def undef(self, tokens):
		self.define(tokens, True)
		return False

	@statement
	def if_(self, tokens):
		if self.states[-1] == ST_SKIPPED:
			self.states.append(ST_SKIPPED)
		elif tokens == ['0']:
			self.states.append(ST_SKIPPED)
		elif tokens == ['1']:
			self.states.append(self.states[-1])
		else:
			self.states.append(ST_UNKNOWN)

		return False

	@statement
	def ifdef(self, tokens, inv = False):
		state = self.states[-1]
		if state == ST_SKIPPED:
			self.states.append(ST_SKIPPED)
			return False
		elif state != ST_INCLUDED or \
		   len(tokens) != 1:
		   	self.states.append(ST_UNKNOWN)
			return False
		define = tokens[0]

		try:
			value = self.defines[define]
		except:
			value = ST_UNKNOWN

		if value == ST_UNKNOWN:
			self.states.append(ST_UNKNOWN)
			return False

		if (value == ST_SKIPPED) ^ inv:
			self.states.append(ST_SKIPPED)
		else:
			self.states.append(ST_INCLUDED)

		return True

	@statement
	def ifndef(self, tokens):
		return self.ifdef(tokens, True)

	@statement
	def else_(self, tokens):
		if len(self.states) == 1:
			print >> sys.stderr, "Warning: #else on top level."
			return False

		state = self.states[-1]
		lastState = self.states[-2]
		if state == ST_SKIPPED:
			self.states[-1] = lastState
		elif state == ST_INCLUDED:
			self.states[-1] = ST_SKIPPED
		else:
			return False
		return True

	@statement
	def endif(self, tokens):
		if len(self.states) == 1:
			print >> sys.stderr, "Warning: #endif underflow."
			return False

		return self.states.pop() != ST_UNKNOWN

	# cleanup
	def finish(self):
		pass

	# remove decorator
	del statement


#
# Main program
#
def syntax(out):
	out.write("Syntax: %s [OPTIONS] [<INPUT>...]\n\n"
	          "    Options:\n"
	          "\t-o, --output=<output>     Write merged files to <output>\n"
	          "\t-h, --help                Show this message\n"
	          "\t-I <include directory>    Add directory to search path\n"
	          "\t-r, --omit-redundant      Omit redundant private header includes\n"
	          "\t-R, --omit-redundant-sys  Omit redundant system header includes\n"
	          "\t-f, --follow              Follow private header includes\n"
	          "\t-F, --follow-sys          Follow system header includes\n"
	          "\t-e, --exchange=<header>   Exchange private includes with <header>\n"
	          "\t-D <def>[=<value>]        Add define keyword\n"
	          "\t-U <def>                  Undefine keyword\n"
	          "\n" % sys.argv[0])

def main(args):
	longOptions = ['output=', 'help', 'omit-redundant', 'omit-redundant-sys',
	               'follow', 'follow-sys', 'exchange=']
	shortOptions = 'o:hI:rRfFD:U:e:'

	try:
		opts, args = getopt.getopt(args, shortOptions, longOptions)
	except getopt.GetoptError:
		syntax(sys.stderr)
		return 1

	outFile = sys.stdout
	includeDirs = [ '.' ]
	flags = Set()
	defines = []
	unDefines = []
	exchangeHeader = None

	for opt in opts:
		if opt[0] in ('-h', '--help'):
			syntax(sys.stdout)
			return 0
		elif opt[0] in ('-o', '--output'):
			try:
				outFile = open(opt[1], 'w')
			except IOError, e:
				print >> sys.stderr,			\
				         "%s opening '%s' for write."	\
				         % (e.strerror, opt[1])
				return 1
		elif opt[0] in ('-I'):
			includeDirs.append(opt[1])
		elif opt[0] in ('-r', '--omit-redundant'):
			flags.add('r')
		elif opt[0] in ('-R', '--omit-redundant-sys'):
			flags.add('R')
		elif opt[0] in ('-f', '--follow'):
			flags.add('f')
		elif opt[0] in ('-F', '--follow-sys'):
			flags.add('F')
		elif opt[0] in ('-e', '--exchange'):
			exchangeHeader = opt[1]
		elif opt[0] in ('-D'):
			defines.append(opt[1])
		elif opt[0] in ('-U'):
			unDefines.append(opt[1])

	inStack = []

	def includeFn(include):
		for dir in includeDirs:
			fileName = os.path.join(dir, include)
			if os.path.exists(fileName):
				break
		else:
			print >> sys.stderr,				\
			         "Warning: Could not find '%s' in"	\
			         " include path." % include
			return False

		try:
			inFile = open(fileName, 'r')
		except IOError, e:
			print >> sys.stderr,				\
			         "Warning: %s opening '%s' for read."	\
			         % (e.strerror, fileName)
			return False

		inStack.append(inFile)
		return True

	merger = Merger(outFile, flags, includeFn)
	if exchangeHeader:
		merger.setExchange(exchangeHeader)
	for define in defines:
		try:
			key, value = define.split('=', 1)
			merger.addDefine(key, value)
		except:
			merger.addDefine(define, "")
	for define in unDefines:
		merger.addUndef(define)

	if len(args) == 0:
		args = ['-']

	for fileName in args:
		if fileName == '-':
			inFile = sys.stdin
		else:
			try:
				inFile = open(fileName, 'r')
			except IOError, e:
				print >> sys.stderr,			\
				         "%s opening '%s' for read."	\
				         % (e.strerror, fileName)
				return 1
		inStack.append(inFile)

		while len(inStack):
			inFile = inStack[-1]
			line = inFile.readline()
			if line == '':
				if inFile != sys.stdin:
					inFile.close()
				inStack.pop()
				continue

			try:
				merger.process(line)
			except:
				# best for now
				raise

	merger.finish()

	if outFile != sys.stdout:
		outFile.close()

	return 0

if __name__ == '__main__':
	sys.exit(main(sys.argv[1:]))
