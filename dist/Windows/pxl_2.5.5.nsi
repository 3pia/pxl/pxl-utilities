!include MUI2.nsh
!include "EnvVarUpdate.nsh"

InstallDirRegKey HKLM "SOFTWARE\PXL" ""

!define MUI_WELCOMEPAGE_TITLE  "Welcome to PXL Installation"
!insertmacro MUI_PAGE_WELCOME 

!define MUI_COMPONENTSPAGE_TEXT_TOP "PXL"
!define MUI_COMPONENTSPAGE_TEXT_COMPLIST "Install Components"

!insertmacro MUI_PAGE_COMPONENTS

!define MUI_DIRECTORYPAGE_TEXT_DESTINATION "Install directory"

Caption "PXL"
InstallDir "$PROGRAMFILES\Pxl"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_TITLE "Installation Complete"
!define MUI_FINISHPAGE_TEXT "PXL installation is complete."
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_COMPONENTS
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"
  
# define the name of the installer
outfile "Pxl 2.5.5 Installer.exe"


# define the directory to install to, the desktop in this case as specified  
# by the predefined $DESKTOP variable
 
# default section

	
Section "PXL 2.5.5" PXL

	SetShellVarContext all

	WriteRegStr HKLM "SOFTWARE\PXL" "" $INSTDIR
	
	# define the output path for this file
	setOutPath $INSTDIR
 
	# define what to install and place it in the output path
	file /r pxl\*.*
	
	Push "PATH" 
	Push "A"
	Push "HKLM"
	Push "$INSTDIR"
	Call EnvVarUpdate
	Pop $1
	WriteUninstaller "$INSTDIR\Uninstall.exe"
	
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PXL" \
					 "DisplayName" "PXL -- Physics eXtension Library"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PXL" \
					 "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
SectionEnd

Section "un.PXL" 

	Push "PATH" 
	Push "R"
	Push "HKCU"
	Push "$INSTDIR"
	Call un.EnvVarUpdate

	DeleteRegKey HKLM "Software\PXL"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PXL"
	RMDir /r "$INSTDIR"
	
SectionEnd





